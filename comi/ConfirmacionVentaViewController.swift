//
//  ConfirmacionVentaViewController.swift
//  comi
//
//  Created by Francisco Escobar on 6/19/20.
//  Copyright © 2020 Francisco Escobar. All rights reserved.
//

import UIKit
import MapKit

class ConfirmacionVentaViewController: UIViewController {

    let puerto = PuertosPlatillo()
    
    var platillo = Platillo()
    
    @IBOutlet weak var fotoVendedorImageView: UIImageView!
    @IBOutlet weak var nombreVendedorLabel: UILabel!
    @IBOutlet weak var enviarMensajeButton: UIButton!
    @IBOutlet weak var verPedidoButton: UIButton!
    @IBOutlet weak var seguirComprandoButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        prepararUI()
    }

    func prepararUI(){
        
        self.navigationItem.title = "Confirmación"
        
        fotoVendedorImageView.layer.cornerRadius = 50
        fotoVendedorImageView.layer.masksToBounds = true
        
        enviarMensajeButton.layer.cornerRadius = 15
        enviarMensajeButton.layer.shadowColor = UIColor(red: 0.1960, green: 0.1960, blue: 0.1960, alpha: 1).cgColor
        enviarMensajeButton.layer.shadowOpacity = 0.3
        enviarMensajeButton.layer.shadowOffset = CGSize(width: 3, height: 3)
        enviarMensajeButton.layer.shadowRadius = 5
        
        verPedidoButton.layer.cornerRadius = 15
        verPedidoButton.layer.shadowColor = UIColor(red: 0.1960, green: 0.1960, blue: 0.1960, alpha: 1).cgColor
        verPedidoButton.layer.shadowOpacity = 0.3
        verPedidoButton.layer.shadowOffset = CGSize(width: 3, height: 3)
        verPedidoButton.layer.shadowRadius = 5
        
        seguirComprandoButton.layer.cornerRadius = 15
        seguirComprandoButton.layer.shadowColor = UIColor(red: 0.1960, green: 0.1960, blue: 0.1960, alpha: 1).cgColor
        seguirComprandoButton.layer.shadowOpacity = 0.3
        seguirComprandoButton.layer.shadowOffset = CGSize(width: 3, height: 3)
        seguirComprandoButton.layer.shadowRadius = 5
        
        if platillo.vendedor.fotoImage == UIImage(){
            puerto.descargarFoto(usuario: platillo.vendedor, en: fotoVendedorImageView)
        }else{
            fotoVendedorImageView.image = platillo.vendedor.fotoImage
        }
        
        nombreVendedorLabel.text = platillo.vendedor.nombre
    }
    
}
