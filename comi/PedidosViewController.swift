//
//  PedidosViewController.swift
//  comi
//
//  Created by Francisco Escobar on 21/07/20.
//  Copyright © 2020 Francisco Escobar. All rights reserved.
//

import UIKit

class PedidosViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    let puertos = PuertosPlatillo()
    var pedido = Pedido()
    
    
    @IBOutlet weak var platillosTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        NotificationCenter.default.addObserver(self, selector: #selector(actualizarListaPlatillos), name: .pedidos, object: nil)
        
        puertos.obtenerPedidos()
        
        let backButton = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        navigationItem.backBarButtonItem = backButton
    }
    
    @objc func actualizarListaPlatillos(_ notification:Notification){
                
        let platillosTopLevel = notification.userInfo as! [String:Any]
        
        pedido = platillosTopLevel["pedido"] as! Pedido
        platillosTableView.reloadData()
    }
    
    
    //MARK:- tableview
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pedido.platillos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let celda = tableView.dequeueReusableCell(withIdentifier: "celdaPlatillo")!
        
        let platillo = pedido.platillos[indexPath.row]
        
        let sombraView = celda.viewWithTag(1)
        let fondoView = celda.viewWithTag(2)
        let platilloImageView = celda.viewWithTag(3) as! UIImageView
        let nombreLabel = celda.viewWithTag(4) as! UILabel
        let totalLabel = celda.viewWithTag(5) as! UILabel
        let cantidadLabel = celda.viewWithTag(6) as! UILabel
        
        sombraView?.layer.cornerRadius = 10
        sombraView?.layer.shadowColor = UIColor(red: 0.1960, green: 0.1960, blue: 0.1960, alpha: 1).cgColor
        sombraView?.layer.shadowOpacity = 0.2
        sombraView?.layer.shadowOffset = CGSize(width: 5, height: 5)
        sombraView?.layer.shadowRadius = 5
        
        fondoView?.layer.cornerRadius = 10
        fondoView?.layer.masksToBounds = true
        
        if platillo.fotoImage == UIImage(){
            
            puertos.descargarFoto(platillo: platillo, en: platilloImageView)
            
        }else{
            platilloImageView.image = platillo.fotoImage
        }
        
        nombreLabel.text = platillo.nombre
        
        if platillo.precio.truncatingRemainder(dividingBy: 1) == 0{
            
            totalLabel.text = String(format: "Total: $%.0f",platillo.precio*Double(platillo.cantidadPedido))
        }else{
        
            totalLabel.text = String(format: "Total: $%.2f",platillo.precio*Double(platillo.cantidadPedido))
        }
        
        cantidadLabel.text = "Cantidad: \(platillo.cantidadPedido)"
        
        celda.selectionStyle = .none
        return celda
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let detallesPedidoVC = storyboard.instantiateViewController(identifier: "detallesPedidosVC") as! DetallesPedidoViewController
        
        detallesPedidoVC.platillo = pedido.platillos[indexPath.row]
        
        self.navigationController?.pushViewController(detallesPedidoVC, animated: true)
    }
}
