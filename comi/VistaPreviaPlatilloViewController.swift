//
//  VistaPreviaPlatilloViewController.swift
//  comi
//
//  Created by Francisco Escobar on 30/06/20.
//  Copyright © 2020 Francisco Escobar. All rights reserved.
//

import UIKit

class VistaPreviaPlatilloViewController: UIViewController {

    
    var repositorioUsuario = RepositorioUsuario()
    var usuario = Usuario()
    
    var platillo = Platillo()
    var cargador = Cargador()
    
    
    @IBOutlet weak var fondoSombraView: UIView!
    @IBOutlet weak var fondoPublicacionView: UIView!
    @IBOutlet weak var platilloImageView: UIImageView!
    @IBOutlet weak var usuarioImageView: UIImageView!
    @IBOutlet weak var nombreUsuarioLabel: UILabel!
    @IBOutlet weak var precioLabel: UILabel!
    @IBOutlet weak var distanciaLabel: UILabel!
    
    @IBOutlet weak var estrella1: UIImageView!
    @IBOutlet weak var estrella2: UIImageView!
    @IBOutlet weak var estrella3: UIImageView!
    @IBOutlet weak var estrella4: UIImageView!
    @IBOutlet weak var estrella5: UIImageView!
    
    
    @IBOutlet weak var cambiarFotoButton: UIButton!
    @IBOutlet weak var cambiarDatosButton: UIButton!
    @IBOutlet weak var publicarPlatilloButton: UIButton!
    @IBOutlet weak var cancelarButton: UIButton!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        prepararUI()
    }
    
    func prepararUI(){
        
        fondoSombraView.layer.cornerRadius = 10
        fondoSombraView.layer.shadowColor = UIColor(red: 0.1960, green: 0.1960, blue: 0.1960, alpha: 1).cgColor
        fondoSombraView.layer.shadowOpacity = 0.2
        fondoSombraView.layer.shadowOffset = CGSize(width: 5, height: 5)
        fondoSombraView.layer.shadowRadius = 5
        
        fondoPublicacionView.layer.cornerRadius = 10
        fondoPublicacionView.layer.masksToBounds = true
        
        platilloImageView.image = platillo.fotoImage
        
        
//        repositorioUsuario.ponerEstrellas(estrella1: estrella1, estrella2: estrella2, estrella3: estrella3, estrella4: estrella4, estrella5: estrella5, usuario: usuario)
        
        usuarioImageView.layer.cornerRadius = 15
        
        
        if platillo.precio.truncatingRemainder(dividingBy: 1) == 0{
            
            precioLabel.text = String(format: "$%.0f",platillo.precio)
        }else{
        
            precioLabel.text = String(format: "$%.2f",platillo.precio)
        }
        
        
        cambiarFotoButton.layer.cornerRadius = 15
        cambiarFotoButton.layer.shadowColor = UIColor(red: 0.1960, green: 0.1960, blue: 0.1960, alpha: 1).cgColor
        cambiarFotoButton.layer.shadowOpacity = 0.3
        cambiarFotoButton.layer.shadowOffset = CGSize(width: 3, height: 3)
        cambiarFotoButton.layer.shadowRadius = 5
        
        cambiarDatosButton.layer.cornerRadius = 15
        cambiarDatosButton.layer.shadowColor = UIColor(red: 0.1960, green: 0.1960, blue: 0.1960, alpha: 1).cgColor
        cambiarDatosButton.layer.shadowOpacity = 0.3
        cambiarDatosButton.layer.shadowOffset = CGSize(width: 3, height: 3)
        cambiarDatosButton.layer.shadowRadius = 5
        
        publicarPlatilloButton.layer.cornerRadius = 15
        publicarPlatilloButton.layer.shadowColor = UIColor(red: 0.1960, green: 0.1960, blue: 0.1960, alpha: 1).cgColor
        publicarPlatilloButton.layer.shadowOpacity = 0.3
        publicarPlatilloButton.layer.shadowOffset = CGSize(width: 3, height: 3)
        publicarPlatilloButton.layer.shadowRadius = 5
        
        let attributedString = NSMutableAttributedString.init(string: "Cancelar")
        attributedString.addAttribute(NSAttributedString.Key.underlineStyle, value: 1, range:
            NSRange.init(location: 0, length: attributedString.length));
        cancelarButton.titleLabel!.attributedText = attributedString
    }
    
    @IBAction func publicar(_ sender: Any) {
        
        cargador = Cargador(viewController: self.tabBarController!)
        
        NotificationCenter.default.addObserver(self, selector: #selector(platilloPublicado), name: .platilloPublicado, object: nil)
        
        let puertos = PuertosPlatillo()
        puertos.publicarPlatillo()
    }
    
    @objc func platilloPublicado(){
        
        cargador.alto()
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let navegacionPrincipal = storyboard.instantiateViewController(identifier: "navigationPrincipal")
        
        navegacionPrincipal.modalPresentationStyle = .fullScreen
        
        self.present(navegacionPrincipal, animated: true, completion: nil)
    }
    
    @IBAction func cambiarDatos(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func cancelar(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let navegacionPrincipal = storyboard.instantiateViewController(identifier: "navigationPrincipal")
        
        navegacionPrincipal.modalPresentationStyle = .fullScreen
        
        self.present(navegacionPrincipal, animated: true, completion: nil)
    }
    
}
