//
//  RealizarPedidoViewController.swift
//  comi
//
//  Created by Francisco Escobar on 6/14/20.
//  Copyright © 2020 Francisco Escobar. All rights reserved.
//

import UIKit
import AudioToolbox

class RealizarPedidoViewController: UIViewController {

    var platillo = Platillo()
    var usuario = Usuario()
    let defaults = UserDefaults.standard
    
    @IBOutlet weak var platilloImageView: UIImageView!
    @IBOutlet weak var sombraPlatilloView: UIView!
    @IBOutlet weak var nombrePlatilloLabel: UILabel!
    @IBOutlet weak var descripcionPlatilloLabel: UILabel!
    @IBOutlet weak var ordenarLabel: UILabel!
    @IBOutlet weak var fondoNumPlatillos: UIView!
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var realizarPedidoButton: UIButton!
    @IBOutlet weak var agregarCarritoButton: UIButton!
    @IBOutlet weak var platilloMasButton: UIButton!
    @IBOutlet weak var platilloMenosButton: UIButton!
    @IBOutlet weak var contadorPlatillosLabel: UILabel!
    
    @IBOutlet weak var carritoView: UIView!
    @IBOutlet weak var cantidadCarritoView: UILabel!
    @IBOutlet weak var totalCarritoView: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        prepararUI()
    }
    
    func prepararUI(){
        
        let backButton = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        navigationItem.backBarButtonItem = backButton
                
        sombraPlatilloView.layer.cornerRadius = 10
        platilloImageView.layer.cornerRadius = 10
        sombraPlatilloView.layer.shadowColor = UIColor(red: 0.1960, green: 0.1960, blue: 0.1960, alpha: 1).cgColor
        sombraPlatilloView.layer.shadowOpacity = 0.3
        sombraPlatilloView.layer.shadowOffset = CGSize(width: 5, height: 5)
        sombraPlatilloView.layer.shadowRadius = 5
        
        platilloImageView.image = platillo.fotoImage
        nombrePlatilloLabel.text = platillo.nombre
        descripcionPlatilloLabel.text = platillo.descripcion
        
        let attributedString = NSMutableAttributedString.init(string: "Ordenar")
        attributedString.addAttribute(NSAttributedString.Key.underlineStyle, value: 1, range:
            NSRange.init(location: 0, length: attributedString.length));
        ordenarLabel.attributedText = attributedString
        
        fondoNumPlatillos.layer.cornerRadius = 13
                
        if platillo.precio.truncatingRemainder(dividingBy: 1) == 0{
            
            totalLabel.text = String(format: "TOTAL: $%.0f",platillo.precio)
        }else{
        
            totalLabel.text = String(format: "TOTAL: $%.2f",platillo.precio)
        }
        
        realizarPedidoButton.layer.cornerRadius = 15
        realizarPedidoButton.layer.shadowColor = UIColor(red: 0.1960, green: 0.1960, blue: 0.1960, alpha: 1).cgColor
        realizarPedidoButton.layer.shadowOpacity = 0.3
        realizarPedidoButton.layer.shadowOffset = CGSize(width: 3, height: 3)
        realizarPedidoButton.layer.shadowRadius = 5
        
        agregarCarritoButton.layer.cornerRadius = 15
        agregarCarritoButton.layer.shadowColor = UIColor(red: 0.1960, green: 0.1960, blue: 0.1960, alpha: 1).cgColor
        agregarCarritoButton.layer.shadowOpacity = 0.3
        agregarCarritoButton.layer.shadowOffset = CGSize(width: 3, height: 3)
        agregarCarritoButton.layer.shadowRadius = 5
        
        carritoView.layer.cornerRadius = 10
        cantidadCarritoView.layer.cornerRadius = 5
        cantidadCarritoView.layer.masksToBounds = true
        
        platilloMenosButton.setTitleColor(.lightGray, for: .normal)
    }

    @IBAction func contrarPlatilloMas(_ sender: Any) {
        
        self.platilloMasButton.layer.removeAllAnimations()
        AudioServicesPlaySystemSound(kSystemSoundID_Vibrate)
        
        var platillos = Int(contadorPlatillosLabel.text!)
        
        if platillos! < platillo.cantidadDisponibles{
            
            platillos! += 1
            
            verificarCantidad(cantidad: platillos!)
            
            UIView.animate(withDuration: 0.2, animations: {
                
                self.contadorPlatillosLabel.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
            }, completion: { termino in
                
                if termino{
                    
                    self.contadorPlatillosLabel.text = "\(platillos ?? 0)"
                    
                    UIView.animate(withDuration: 0.2, animations: {
                        
                        self.contadorPlatillosLabel.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                    })
                }
            })
        }
        
    }
    
    @IBAction func contrarPlatilloMenos(_ sender: Any) {
        
        self.contadorPlatillosLabel.layer.removeAllAnimations()
        AudioServicesPlaySystemSound(kSystemSoundID_Vibrate)
        
        var platillos = Int(contadorPlatillosLabel.text!)

        if platillos! > 1{
            
            platillos! -= 1
            
            verificarCantidad(cantidad: platillos!)
            
            UIView.animate(withDuration: 0.2, animations: {
                
                self.contadorPlatillosLabel.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
            }, completion: { termino in
                
                if termino{
                    
                    self.contadorPlatillosLabel.text = "\(platillos ?? 0)"
                    
                    UIView.animate(withDuration: 0.2, animations: {
                        
                        self.contadorPlatillosLabel.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                    })
                }
            })
        }
    }
    
    func verificarCantidad(cantidad:Int){
        
        if cantidad == 1{
                
            platilloMenosButton.setTitleColor(.lightGray, for: .normal)
            platilloMasButton.setTitleColor(UIColor(red: 0.325490, green: 0.325490, blue: 0.325490, alpha: 1), for: .normal)
                        
        }else if cantidad == platillo.cantidadDisponibles{
            
            platilloMasButton.setTitleColor(.lightGray, for: .normal)
            platilloMenosButton.setTitleColor(UIColor(red: 0.325490, green: 0.325490, blue: 0.325490, alpha: 1), for: .normal)
            
        }else{
            
            platilloMasButton.setTitleColor(UIColor(red: 0.325490, green: 0.325490, blue: 0.325490, alpha: 1), for: .normal)
            platilloMenosButton.setTitleColor(UIColor(red: 0.325490, green: 0.325490, blue: 0.325490, alpha: 1), for: .normal)
        }
                            
        if (platillo.precio*Double(cantidad)).truncatingRemainder(dividingBy: 1) == 0{
            
            totalLabel.text = String(format: "TOTAL: $%.0f",platillo.precio*Double(cantidad))
        }else{
        
            totalLabel.text = String(format: "TOTAL: $%.2f",platillo.precio*Double(cantidad))
        }
    }
    
    @IBAction func agregarAlCarrito(_ sender: Any) {
                
        self.tabBarController?.view.addSubview(carritoView)
        
        NSLayoutConstraint.activate([carritoView.centerXAnchor.constraint(equalTo: (self.tabBarController?.view.centerXAnchor)!), carritoView.widthAnchor.constraint(equalTo: (self.tabBarController?.view.widthAnchor)!, multiplier: 0.9), carritoView.bottomAnchor.constraint(equalTo: (self.tabBarController?.view.bottomAnchor)!, constant: -(self.tabBarController?.tabBar.frame.height)!-10)])
                
        let cantidadPlatillos = Int(contadorPlatillosLabel.text!)
        
        if cantidadPlatillos! > platillo.cantidadDisponibles{
            
            let alertController = UIAlertController(title: "", message: "Sólo quedan \(platillo.cantidadDisponibles) disponibles", preferredStyle: .alert)
            let aceptarAction = UIAlertAction(title: "Aceptar", style: .default, handler: nil)
            
            alertController.addAction(aceptarAction)
            
            self.present(alertController, animated: true, completion: nil)
        }else{
            platillo.cantidadPedido = cantidadPlatillos!
            
            let repositorioCarrito = RepositorioCarrito()
            repositorioCarrito.agregarAlCarrito(platillo: platillo)
            let carrito = repositorioCarrito.obtenerCarrito()
                        
            carritoView.isHidden = false
            cantidadCarritoView.text = "\(repositorioCarrito.calcularCantidad())"
            
            totalCarritoView.text = "$\(repositorioCarrito.calcularTotal())"
            
            platillo.cantidadDisponibles = platillo.cantidadDisponibles-cantidadPlatillos!
        }
    }
    
    
    //MARK: - unwind
    
    @IBAction func unwindContinuarVenta(segue: UIStoryboardSegue){
                        
        if let segue = segue as? UIStoryboardSegueWithCompletion{
            
            segue.completion = {
                self.performSegue(withIdentifier: "continuarSegue", sender: self)
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "continuarSegue"{
            
            let confirmacionVentaVC = segue.destination as! ConfirmacionVentaViewController
            
            confirmacionVentaVC.platillo = platillo
        }
    }
}
