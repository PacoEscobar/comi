//
//  RepositorioCarrito.swift
//  comi
//
//  Created by Francisco Escobar on 11/07/20.
//  Copyright © 2020 Francisco Escobar. All rights reserved.
//

import Foundation
import UIKit

class RepositorioCarrito{
    
    let defaults = UserDefaults.standard
    
    func agregarAlCarrito(platillo:Platillo){
        
        var carritoData = defaults.data(forKey: "carrito")
        var carrito:[[String:Any]] = [[String:Any]]()
        
        if carritoData != nil{
            do{
                
                carrito = try JSONSerialization.jsonObject(with: carritoData!, options: .mutableContainers) as! [[String : Any]]
            }catch{}
        }
        carrito.append(platilloToJson(platillo: platillo))
        
        do{
            
            carritoData = try JSONSerialization.data(withJSONObject: carrito, options: .fragmentsAllowed)
        }catch{}
        
        defaults.set(carritoData, forKey: "carrito")
                
    }
    
    func obtenerCarrito()->Carrito{
        
        let carritoData = defaults.data(forKey: "carrito")
                
        var carritoJson:[[String:Any]] = [[String:Any]]()
        var platillos = [Platillo]()
        var carrito = Carrito()
        
        do{
            
            carritoJson = try JSONSerialization.jsonObject(with: carritoData!, options: .mutableContainers) as! [[String : Any]]
            
        }catch{}
        
        for elementoCarrito in carritoJson{
            
            platillos.append(jsonToPlatillo(json: elementoCarrito))
        }
        
        carrito = arrayPlatilloToCarrito(platillos: platillos)
        
        return carrito
    }
    
    func platilloToJson(platillo:Platillo) -> [String:Any]{
        
        var json:[String: Any] = [String:Any]()

        let fotoImageString = "\(platillo.fotoImage.jpegData(compressionQuality: 1)!.base64EncodedString())"
                
        json["id"] = platillo.id
        json["nombre"] = platillo.nombre
        json["descripcion"] = platillo.descripcion
        json["foto"] = platillo.foto
        json["fotoImage"] = fotoImageString
        json["entrega"] = platillo.entrega
        json["precio"] = platillo.precio
        json["cantidad"] = platillo.cantidadDisponibles
        json["cantidadPedido"] = platillo.cantidadPedido
        json["fechaPreparacion"] = platillo.fechaPreparacion
        json["ubicacion"] = platillo.ubicacion
        json["esParaPedir"] = platillo.esParaPedir
        json["horaDeEntrega"] = platillo.horaDeEntrega
        json["horaLimitePedidos"] = platillo.horaLimitePedidos
                
        return json
    }
    
    func jsonToPlatillo(json:[String:Any])->Platillo{
        
        let platillo = Platillo()
        let fotoImageData = Data(base64Encoded: json["fotoImage"] as! String)
        let fotoImage = UIImage(data: fotoImageData!)
        
        platillo.id = json["id"] as! Int
        platillo.nombre = json["nombre"] as! String
        platillo.descripcion = json["descripcion"] as! String
        platillo.foto = json["foto"] as! String
        platillo.fotoImage = fotoImage!
        platillo.entrega = json["entrega"] as! Int
        platillo.precio = json["precio"] as! Double
        platillo.cantidadDisponibles = json["cantidad"] as! Int
        platillo.cantidadPedido = json["cantidadPedido"] as! Int
        platillo.fechaPreparacion = json["fechaPreparacion"] as! Double
        platillo.ubicacion = json["ubicacion"] as! NSMutableDictionary
        platillo.esParaPedir = json["esParaPedir"] as! Bool
        platillo.horaDeEntrega = json["horaDeEntrega"] as! Double
        platillo.horaLimitePedidos = json["horaLimitePedidos"] as! Double
        
        return platillo
    }
    
    func arrayPlatilloToCarrito(platillos:[Platillo])->Carrito{
        
        let carrito = Carrito()
        
        carrito.platillos = platillos
        carrito.cantidad = platillos.count
        
        return carrito
    }
    
    func calcularCantidad()->Int{
        
        let platillos = obtenerCarrito()
        
        var cantidad = 0
        
        for platillo in platillos.platillos{
            
            cantidad += platillo.cantidadPedido
            print("cantidad platillo \(platillo.cantidadPedido)")
        }
        
        return cantidad
    }
    
    func calcularTotal()->Double{
        
        let platillos = obtenerCarrito()
        
        var total = 0.0
        
        for platillo in platillos.platillos{
            
            total += (platillo.precio*Double(platillo.cantidadPedido))
        }
        
        return total
    }
}
