//
//  Platillo.swift
//  comi
//
//  Created by Francisco Escobar on 6/5/20.
//  Copyright © 2020 Francisco Escobar. All rights reserved.
//

import Foundation
import UIKit

class Platillo{
    
    var id = 0
    var nombre = ""
    var descripcion = ""
    var foto = ""
    var entrega = 0
    var precio = 0.0
    var cantidadDisponibles = 0
    var cantidadPedido = 0
    var fechaPreparacion = 0.0
    var ubicacion = NSMutableDictionary()
    var vendedor = Usuario()
    
    var fotoImage = UIImage()
    
    //Campos platillos de pedido
    
    var esParaPedir = false
    var horaDeEntrega = 0.0
    var horaLimitePedidos = 0.0
}
