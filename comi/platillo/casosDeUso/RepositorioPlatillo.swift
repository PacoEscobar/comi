//
//  RepositorioPlatillo.swift
//  comi
//
//  Created by Francisco Escobar on 01/07/20.
//  Copyright © 2020 Francisco Escobar. All rights reserved.
//

import Foundation
import UIKit

class RepositorioPlatillo{
    
    func validarCamposLlenos(nombrePlatilloTextField: UITextField, descripcionPlatilloTextView: UITextView, cantidadPlatillosTextField: UITextField, precioPlatillosTextField:UITextField) -> [Int]{
        
        var faltantes: [Int] = [Int]()
        
        if nombrePlatilloTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == ""{
            
            faltantes.append(1)
        }
        if descripcionPlatilloTextView.text.trimmingCharacters(in: .whitespacesAndNewlines) == ""{
            
            faltantes.append(2)
        }
        
        if cantidadPlatillosTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == ""{
            
            faltantes.append(3)
        }
        
        if precioPlatillosTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == ""{
            
            faltantes.append(4)
        }
        
        return faltantes
    }
    
    func validarCantidad(textField: UITextField)->Bool{
        
        let textoTextField = Int(textField.text!)
        
        if textoTextField == nil{
            
            return false
        }else{

            return true
        }
    }
    
    func validarNumeros(textField: UITextField)->Bool{
        
        let textoTextField = Double(textField.text!)
        
        if textoTextField == nil{
            
            return false
        }else{

            return true
        }
    }
    
    func coloresDefault(textField: UITextField){
        
        textField.backgroundColor = .white
        textField.layer.borderWidth = UITextField().layer.borderWidth
    }
    
    func coloresError(textField: UITextField){
        
        textField.backgroundColor = UIColor(red: 245, green: 138, blue: 105, a: 1)
        textField.layer.borderWidth = 2
        textField.layer.borderColor = UIColor.red.cgColor
    }
    
    func calcularTotal(platillo: Platillo)->Double{
        
        var total = 0.0
        
        total = Double(platillo.cantidadPedido)*platillo.precio
        
        return total
    }
    
    func calcularTiempoRestantePlatillo(platillo:Platillo)->String{
        
        let tiempoActualUnix = Date().timeIntervalSince1970
        let tiempoRestante = platillo.horaLimitePedidos-tiempoActualUnix
        var tiempoRestanteString = ""
        print("tiempo restante \(platillo.horaLimitePedidos)")
        if tiempoRestante < 60{
            
            tiempoRestanteString = "\(tiempoRestante) segundos"
        }else if tiempoRestante >= 60 && tiempoRestante < 3600{
            
            let tiempoRestanteInt = Int(tiempoRestante/60)
            
            tiempoRestanteString = "\(tiempoRestanteInt) minutos"
        }else if tiempoRestante >= 3600 && tiempoRestante < 86400{
            
            let tiempoRestanteInt = Int(tiempoRestante/3600)
            
            tiempoRestanteString = "\(tiempoRestanteInt) horas"
        }else if tiempoRestante >= 86400{
            
            let tiempoRestanteInt = Int(tiempoRestante/86400)
            
            tiempoRestanteString = "\(tiempoRestanteInt) días"
        }
        
        return tiempoRestanteString
        
    }
}
