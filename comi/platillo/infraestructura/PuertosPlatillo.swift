//
//  Puertos.swift
//  comi
//
//  Created by Francisco Escobar on 6/7/20.
//  Copyright © 2020 Francisco Escobar. All rights reserved.
//

import Foundation
import UIKit

class PuertosPlatillo{
    
    func obtenerPlatillos(){
        
        var listaPlatillosListos: [Platillo] = [Platillo]()
        var listaPlatillosProgramados:[Platillo] = [Platillo]()
        
        let platillo = Platillo()
        let usuario = Usuario()
        
       // platillo.foto = "https://cdn2.cocinadelirante.com/sites/default/files/styles/gallerie/public/images/2019/09/salsa-para-tortas-ahogada.jpg"
        platillo.foto = "https://scontent.fbjx1-1.fna.fbcdn.net/v/t1.15752-9/116512521_3344770392210639_772353259990014371_n.jpg?_nc_cat=108&_nc_sid=b96e70&_nc_ohc=6pOj9tmLaa0AX8VH867&_nc_ht=scontent.fbjx1-1.fna&oh=d7d33f3da3f17c591e490152f489d8cf&oe=5F484488"
        platillo.nombre = "Torta ahogada"
        platillo.precio = 25
        platillo.cantidadDisponibles = 10
        platillo.descripcion = "Torta ahogada como la hacía tu mamá en Guadalajara"
        platillo.ubicacion = ["latitud":22.761357,"longitud":-102.533514]
        
        usuario.foto = "https://image.freepik.com/foto-gratis/angulo-hijo-papa-cocinando_23-2148366187.jpg"
        usuario.nombre = "Carlos Parra"
        usuario.calificacion = 5.0
        
        platillo.vendedor = usuario
        
        listaPlatillosListos.append(platillo)
        
        let platillo3 = Platillo()
        let usuario3 = Usuario()
        
       // platillo3.foto = "https://cdn2.cocinadelirante.com/sites/default/files/styles/gallerie/public/images/2018/06/chilaquiles-rojos-receta-facil-pollo-huevo-carne-asada-ingredientes.jpg"
        platillo3.foto = "https://scontent.fbjx1-1.fna.fbcdn.net/v/t1.15752-9/116223616_289196632156530_291524358353546124_n.jpg?_nc_cat=110&_nc_sid=b96e70&_nc_ohc=Fnt69eT2fKsAX-iNaaT&_nc_ht=scontent.fbjx1-1.fna&oh=323f9a5763c9af1e3fea4f22799f8ff3&oe=5F482281"
        platillo3.nombre = "Chilaquiles"
        platillo3.precio = 60.50
        platillo3.cantidadDisponibles = 2
        platillo3.descripcion = "Muy ricos, picositos pero sabrosos"
        platillo3.ubicacion = ["latitud":22.761357,"longitud":-102.533514]
        
        usuario3.foto = "https://image.freepik.com/foto-gratis/angulo-hijo-papa-cocinando_23-2148366187.jpg"
        usuario3.nombre = "Carlos Parra"
        usuario3.calificacion = 4.0
        
        platillo3.vendedor = usuario3
        
        listaPlatillosListos.append(platillo3)
        
        
        let platillo2 = Platillo()
        let usuario2 = Usuario()
        
        //platillo2.foto = "https://okdiario.com/img/2016/05/14/receta-de-chuletas-a-la-castellana-2-655x368.jpg"
        platillo2.foto = "https://scontent.fbjx1-1.fna.fbcdn.net/v/t1.15752-9/115977778_320987789079425_8999599862597455158_n.jpg?_nc_cat=104&_nc_sid=b96e70&_nc_ohc=l3He62rvRVMAX8WaCac&_nc_ht=scontent.fbjx1-1.fna&oh=342f6fddb44577bc2c93cdce73fa7834&oe=5F49E0DC"
        platillo2.nombre = "Chuletas a la castellana asfd asf"
        platillo2.precio = 30
        platillo2.cantidadDisponibles = 8
        platillo2.descripcion = "Estilo Español, perronsotas"
        platillo2.ubicacion = ["latitud":22.761357,"longitud":-102.533514]
        platillo2.esParaPedir = true
        platillo2.horaDeEntrega = 1595871000
        platillo2.horaLimitePedidos = 1596164400
        
        usuario2.foto = "https://image.freepik.com/foto-gratis/angulo-hijo-papa-cocinando_23-2148366187.jpg"
        usuario2.nombre = "Carlos Parra"
        usuario2.calificacion = 2.0
        
        platillo2.vendedor = usuario2
        
        listaPlatillosProgramados.append(platillo2)
        
        
        
        
        let platillo4 = Platillo()
        let usuario4 = Usuario()
        
//        platillo4.foto = "https://sevilla.abc.es/gurme/wp-content/uploads/sites/24/2012/01/comida-rapida-casera.jpg"
        platillo4.foto = "https://scontent.fbjx1-1.fna.fbcdn.net/v/t1.15752-9/116263211_228602438272798_7310963507307356472_n.jpg?_nc_cat=109&_nc_sid=b96e70&_nc_ohc=6APt1DooMJAAX9OfP3n&_nc_ht=scontent.fbjx1-1.fna&oh=90af8f65b9693e7f45f0ffeac0db28b1&oe=5F4A2DFB"
        platillo4.nombre = "Hamburguesa"
        platillo4.precio = 50
        platillo4.cantidadDisponibles = 15
        platillo4.descripcion = "Hamburguesa casera, gordita y jugocita"
        platillo4.ubicacion = ["latitud":22.761357,"longitud":-102.533514]
        platillo4.esParaPedir = true
        platillo4.horaDeEntrega = (1595871000+7200)
        platillo4.horaLimitePedidos = 1596171600
        
        usuario4.foto = "https://image.freepik.com/foto-gratis/angulo-hijo-papa-cocinando_23-2148366187.jpg"
        usuario4.nombre = "Carlos Parra"
        usuario4.calificacion = 4.0
        
        platillo4.vendedor = usuario4
        
        listaPlatillosProgramados.append(platillo4)
        
        
        
        
        
        
        
        let platillo5 = Platillo()
        let usuario5 = Usuario()
        
//        platillo5.foto = "https://assets.tmecosys.com/image/upload/t_web767x639/img/recipe/ras/Assets/0749D9BC-260D-40F4-A07F-54814C4A82B4/Derivates/A73A7793-F3EE-4B90-ABA4-1CC1A0C3E18F.jpg"
        platillo5.foto = "https://scontent.fbjx1-1.fna.fbcdn.net/v/t1.15752-9/116429337_957377024759419_4657973665279990077_n.jpg?_nc_cat=103&_nc_sid=b96e70&_nc_ohc=HATIyJ_dH8YAX849Ke6&_nc_ht=scontent.fbjx1-1.fna&oh=390a92ced11386c04310c5788d0a2aca&oe=5F475E8B"
        platillo5.nombre = "Sushi de camarón"
        platillo5.ubicacion = ["latitud":22.761357,"longitud":-102.533514]
        platillo5.esParaPedir = true
        platillo5.horaDeEntrega = (1595871000+10800)
        platillo5.horaLimitePedidos = (1595865600+10800)
        
        usuario5.foto = "https://image.freepik.com/foto-gratis/angulo-hijo-papa-cocinando_23-2148366187.jpg"
        usuario5.nombre = "Carlos Parra"
        usuario5.calificacion = 4.0
        
        platillo5.vendedor = usuario5
        
        listaPlatillosProgramados.append(platillo5)
        
        
        let platillos: [String: Any] = ["platillosListos":listaPlatillosListos, "platillosProgramados":listaPlatillosProgramados]
        
        NotificationCenter.default.post(name:.listaPlatillos, object: nil, userInfo: platillos)
        
    }
    
    func vender(){
                
        NotificationCenter.default.post(name:.platilloVendido, object: nil, userInfo: nil)
    }
    
    func publicarPlatillo(){
        
        NotificationCenter.default.post(name:.platilloPublicado, object: nil, userInfo: nil)
    }
    
    func obtenerPedidos(){
        
        let pedido = Pedido()
        let platillo = Platillo()
        let usuario = Usuario()
        
        platillo.foto = "https://cdn2.cocinadelirante.com/sites/default/files/styles/gallerie/public/images/2019/09/salsa-para-tortas-ahogada.jpg"
        platillo.nombre = "Torta ahogada"
        platillo.precio = 25
        platillo.cantidadDisponibles = 10
        platillo.cantidadPedido = 5
        platillo.entrega = 2
        platillo.descripcion = "Torta ahogada como la hacía tu mamá en Guadalajara"
        platillo.ubicacion = ["latitud":22.761357,"longitud":-102.533514]
        
        usuario.foto = "https://image.freepik.com/foto-gratis/angulo-hijo-papa-cocinando_23-2148366187.jpg"
        usuario.nombre = "Carlos Parra"
        usuario.calificacion = 5.0
        
        platillo.vendedor = usuario
        pedido.platillos.append(platillo)
        
        let platillo3 = Platillo()
        let usuario3 = Usuario()
        
        platillo3.foto = "https://cdn2.cocinadelirante.com/sites/default/files/styles/gallerie/public/images/2018/06/chilaquiles-rojos-receta-facil-pollo-huevo-carne-asada-ingredientes.jpg"
        platillo3.nombre = "Chilaquiles"
        platillo3.precio = 60.50
        platillo3.cantidadDisponibles = 2
        platillo3.cantidadPedido = 1
        platillo3.entrega = 1
        platillo3.descripcion = "Muy ricos, picositos pero sabrosos"
        platillo3.ubicacion = ["latitud":22.761357,"longitud":-102.533514]
        
        usuario3.foto = "https://image.freepik.com/foto-gratis/angulo-hijo-papa-cocinando_23-2148366187.jpg"
        usuario3.nombre = "Carlos Parra"
        usuario3.calificacion = 4.0
        
        platillo3.vendedor = usuario3
        
        pedido.platillos.append(platillo3)
        
        let platillos: [String: Any] = ["pedido":pedido]

        NotificationCenter.default.post(name:.pedidos, object: nil, userInfo: platillos)
    }
    
    func obtenerEntregas(){
        
        var listaPlatillos: [[String: Any]] = [[String: Any]]()
        
        let platillo = Platillo()
        let usuario = Usuario()
        
        platillo.foto = "https://cdn2.cocinadelirante.com/sites/default/files/styles/gallerie/public/images/2019/09/salsa-para-tortas-ahogada.jpg"
        platillo.nombre = "Torta ahogada"
        platillo.precio = 25
        platillo.cantidadDisponibles = 10
        platillo.entrega = 2
        platillo.descripcion = "Torta ahogada como la hacía tu mamá en Guadalajara"
        platillo.ubicacion = ["latitud":22.761357,"longitud":-102.533514]
        
        usuario.foto = "https://image.freepik.com/foto-gratis/angulo-hijo-papa-cocinando_23-2148366187.jpg"
        usuario.nombre = "Carlos Parra"
        usuario.calificacion = 5.0
        
        listaPlatillos.append(["platillo": platillo, "usuario": usuario])
        
        let platillo3 = Platillo()
        let usuario3 = Usuario()
        
        platillo3.foto = "https://cdn2.cocinadelirante.com/sites/default/files/styles/gallerie/public/images/2018/06/chilaquiles-rojos-receta-facil-pollo-huevo-carne-asada-ingredientes.jpg"
        platillo3.nombre = "Chilaquiles"
        platillo3.precio = 60.50
        platillo3.cantidadDisponibles = 2
        platillo3.entrega = 1
        platillo3.descripcion = "Muy ricos, picositos pero sabrosos"
        platillo3.ubicacion = ["latitud":22.761357,"longitud":-102.533514]
        
        usuario3.foto = "https://image.freepik.com/foto-gratis/angulo-hijo-papa-cocinando_23-2148366187.jpg"
        usuario3.nombre = "Carlos Parra"
        usuario3.calificacion = 4.0
        
        listaPlatillos.append(["platillo": platillo3, "usuario": usuario3])
        
        let platillos: [String: Any] = ["platillos":listaPlatillos]

        NotificationCenter.default.post(name:.entregas, object: nil, userInfo: platillos)
    }
    
    //MARK:- fotos
    
    func descargarFoto(platillo:Platillo, en: UIImageView){
                
        descargarImagen(from: platillo.foto, completionHandler: {data in
            
            platillo.fotoImage = UIImage(data: data!)!
            DispatchQueue.main.async {
                
                en.image = self.resizeImagen(imagen: platillo.fotoImage)
            }
        })
    }
    
    func descargarFoto(platillo:Platillo, width: CGFloat, height: CGFloat, en: UIImageView){
                
        descargarImagen(from: platillo.foto, completionHandler: {data in
            
            platillo.fotoImage = UIImage(data: data!)!
            DispatchQueue.main.async {
                
                en.image = self.resizeImagen(imagen: platillo.fotoImage, width: width, height: height)
            }
        })
    }
    
    func descargarFoto(usuario:Usuario, en: UIImageView){
        
        descargarImagen(from: usuario.foto, completionHandler: {data in
            
            usuario.fotoImage = UIImage(data: data!)!
            DispatchQueue.main.async {
                
                en.image = usuario.fotoImage
            }
        })
    }
    
    func descargarImagen(from urlString: String, completionHandler: @escaping (_ data: Data?) -> ()) {
        let session = URLSession.shared
        let url = URL(string: urlString)
            
        let dataTask = session.dataTask(with: url!) { (data, response, error) in
            if error != nil {
                print("Error fetching the image! 😢")
                completionHandler(nil)
            } else {
                completionHandler(data)
            }
        }
            
        dataTask.resume()
    }
    
    func resizeImagen(imagen: UIImage)->UIImage{
        
        let porcentajeHeight = (200*100)/imagen.size.height
        
        let widthNuevo = (imagen.size.width*porcentajeHeight)/100
        
        let size = CGSize(width: widthNuevo, height: 200)
        UIGraphicsBeginImageContextWithOptions(size, false, 1.0)
        imagen.draw(in: CGRect(x: 0, y: 0, width: widthNuevo, height:200))
        let imagenNueva = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return imagenNueva!
    }
    
    func resizeImagen(imagen: UIImage, width: CGFloat, height: CGFloat)->UIImage{
        
        let porcentajeHeight = (height*100)/imagen.size.height
        
        let widthNuevo = (imagen.size.width*porcentajeHeight)/100
        
        let size = CGSize(width: widthNuevo, height: height)
        UIGraphicsBeginImageContextWithOptions(size, false, 1.0)
        imagen.draw(in: CGRect(x: 0, y: 0, width: widthNuevo, height:height))
        let imagenNueva = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return imagenNueva!
    }
}
