//
//  NombresNotificacionPlatillo.swift
//  comi
//
//  Created by Francisco Escobar on 6/7/20.
//  Copyright © 2020 Francisco Escobar. All rights reserved.
//

import Foundation

public extension Notification.Name{
    
    static let listaPlatillos = Notification.Name("listaPlatillos")
    static let platilloVendido = Notification.Name("platilloVendido")
    static let platilloPublicado = Notification.Name("platilloPublicado")
    static let entregas = Notification.Name("entregas")
    static let pedidos = Notification.Name("pedidos")
    static let mensajes = Notification.Name("mensajes")
    static let tarjetas = Notification.Name("tarjetas")
    static let addTarjeta = Notification.Name("addTarjeta")
}
