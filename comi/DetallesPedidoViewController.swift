//
//  DetallesPedidoViewController.swift
//  comi
//
//  Created by Francisco Escobar on 21/07/20.
//  Copyright © 2020 Francisco Escobar. All rights reserved.
//

import UIKit
import MapKit

class DetallesPedidoViewController: UIViewController, MKMapViewDelegate {

    var usuario = Usuario()
    
    var platillo = Platillo()
    let puertosPlatillo = PuertosPlatillo()
    
    @IBOutlet weak var fondoView: UIView!
    @IBOutlet weak var sombraView: UIView!
    @IBOutlet weak var platilloImageView: UIImageView!
    @IBOutlet weak var platilloLabel: UILabel!
    @IBOutlet weak var precioLabel: UILabel!
    @IBOutlet weak var cantidadLabel: UILabel!
    @IBOutlet weak var enviarMensajeButton: UIButton!
    
    @IBOutlet weak var mapView: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        prepararUI()
    }
    
    func prepararUI(){
        
        let repositorioUsurario = RepositorioUsuario()
        usuario = repositorioUsurario.obtenerMisDatos()
        
        sombraView?.layer.cornerRadius = 10
        sombraView?.layer.shadowColor = UIColor(red: 0.1960, green: 0.1960, blue: 0.1960, alpha: 1).cgColor
        sombraView?.layer.shadowOpacity = 0.2
        sombraView?.layer.shadowOffset = CGSize(width: 5, height: 5)
        sombraView?.layer.shadowRadius = 5
        
        fondoView?.layer.cornerRadius = 10
        fondoView?.layer.masksToBounds = true
        
        if platillo.fotoImage == UIImage(){
            
            puertosPlatillo.descargarFoto(platillo: platillo, en: platilloImageView)
        }else{
            
            platilloImageView.image = platillo.fotoImage
        }
        
        platilloLabel.text = platillo.nombre
        
        if platillo.precio.truncatingRemainder(dividingBy: 1) == 0{
            
            precioLabel.text = String(format: "Total: $%.0f",platillo.precio*Double(platillo.cantidadPedido))
        }else{
            
            precioLabel.text = String(format: "Total:$%.2f",platillo.precio*Double(platillo.cantidadPedido))
        }
        
        cantidadLabel.text = "Cantidad: \(platillo.cantidadPedido)"
        
        enviarMensajeButton.layer.cornerRadius = 15
        enviarMensajeButton.layer.shadowColor = UIColor(red: 0.1960, green: 0.1960, blue: 0.1960, alpha: 1).cgColor
        enviarMensajeButton.layer.shadowOpacity = 0.3
        enviarMensajeButton.layer.shadowOffset = CGSize(width: 3, height: 3)
        enviarMensajeButton.layer.shadowRadius = 5
        
        let location = CLLocation(latitude: platillo.ubicacion.object(forKey: "latitud") as! Double, longitude: platillo.ubicacion.object(forKey: "longitud") as! Double)
        
        let region = MKCoordinateRegion(center: location.coordinate, latitudinalMeters: 1000, longitudinalMeters: 1000)
        
        mapView.setRegion(region, animated: true)
        
        let point = MKPointAnnotation()
        
        point.coordinate = CLLocationCoordinate2D(latitude: platillo.ubicacion.object(forKey: "latitud") as! Double, longitude: platillo.ubicacion.object(forKey: "longitud") as! Double)
        
        mapView.addAnnotation(point)
        
        let backButton = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        navigationItem.backBarButtonItem = backButton
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.destination is MensajesViewController{
            
            let mensajesVC = segue.destination as! MensajesViewController
            
            mensajesVC.yo = usuario
            mensajesVC.otro = platillo.vendedor
        }
    }
    
    // MARK:- mapview
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        let identifier = "ubicacionPlatillo"
        
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
        
        if annotationView == nil{
            
            annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            annotationView?.canShowCallout = true
            annotationView?.image = UIImage(named: "pin_bicicleta")
        }else{
            
            annotationView?.annotation = annotation
        }
        
        return annotationView
    }
}
