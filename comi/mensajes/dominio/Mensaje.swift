//
//  Mensaje.swift
//  comi
//
//  Created by Francisco Escobar on 20/07/20.
//  Copyright © 2020 Francisco Escobar. All rights reserved.
//

import Foundation

class Mensaje{
    
    var id = 0
    var texto = ""
    var owner = 0 // 0 - de la otra parte, 1 - mio
    var fecha:Double = 0.0
}
