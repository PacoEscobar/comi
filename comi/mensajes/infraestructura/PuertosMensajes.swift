//
//  PuertosMensajes.swift
//  comi
//
//  Created by Francisco Escobar on 20/07/20.
//  Copyright © 2020 Francisco Escobar. All rights reserved.
//

import Foundation

class PuertosMensajes{
    
    func obtenerMensajes(yo: Usuario, otro: Usuario){
        
        var mensajes = [Mensaje]()
        
        let mensaje = Mensaje()
        
        mensaje.owner = 0
        mensaje.texto = "Va en camino su platillo joven"
        mensaje.fecha = 1595283296
        
        mensajes.append(mensaje)
        
        let mensaje1 = Mensaje()
        
        mensaje1.owner = 1
        mensaje1.texto = "Sale! gracias!"
        mensaje1.fecha = 1595283296
        
        mensajes.append(mensaje1)
        
        let mensaje2 = Mensaje()
        
        mensaje2.owner = 1
        mensaje2.texto = "Si le puso salsa verdad?"
        mensaje2.fecha = 1595283296
        
        mensajes.append(mensaje2)
        
        let mensaje3 = Mensaje()
        
        mensaje3.owner = 0
        mensaje3.texto = "Si joven, chilena"
        mensaje3.fecha = 1595283296
        
        mensajes.append(mensaje3)
        
        let mensaje4 = Mensaje()
        
        mensaje4.owner = 1
        mensaje4.texto = "Aaaa va, gracias"
        mensaje4.fecha = 1595283296
        
        mensajes.append(mensaje4)
        
        let userInfoMensajes:[String:Any] = ["mensajes":mensajes]
        
        NotificationCenter.default.post(name:.mensajes, object: nil, userInfo: userInfoMensajes)
    }
}
