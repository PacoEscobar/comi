//
//  RepositorioPedido.swift
//  comi
//
//  Created by Francisco Escobar on 21/07/20.
//  Copyright © 2020 Francisco Escobar. All rights reserved.
//

import Foundation

class RepositorioPedido{
    
    func calcularTotal(pedido: Pedido)->Double{
        
        var total = 0.0
        
        for platillo in pedido.platillos {
            
            total = total+platillo.precio
        }
        
        return total
    }
    
    func calcularCantidad(pedido: Pedido)->Int{
        
        return pedido.platillos.count
    }
}
