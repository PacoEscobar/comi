//
//  NoPlatillosViewController.swift
//  comi
//
//  Created by Francisco Escobar on 19/07/20.
//  Copyright © 2020 Francisco Escobar. All rights reserved.
//

import UIKit

class NoPlatillosViewController: UIViewController {

    let usuario = Usuario()
    
    @IBOutlet weak var noPlatillosCaraView: UIView!
    @IBOutlet weak var convierteteVendedorButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        prepararUI()
    }
    
    func prepararUI(){

        self.navigationItem.title = "Platillos"
        
        let botonMenu = UIBarButtonItem(image: #imageLiteral(resourceName: "burguer"), style: .done, target: self, action: #selector(abrirMenu))
        self.navigationItem.leftBarButtonItem = botonMenu
        
        noPlatillosCaraView.layer.cornerRadius = 40
        noPlatillosCaraView.layer.shadowColor = UIColor(red: 0.1960, green: 0.1960, blue: 0.1960, alpha: 1).cgColor
        noPlatillosCaraView.layer.shadowOpacity = 0.2
        noPlatillosCaraView.layer.shadowOffset = CGSize(width: 2, height: 5)
        noPlatillosCaraView.layer.shadowRadius = 5
        
        convierteteVendedorButton.titleLabel?.numberOfLines = 0
        convierteteVendedorButton.titleLabel?.textAlignment = .center
        convierteteVendedorButton.contentEdgeInsets = UIEdgeInsets(top: 15, left: 5, bottom: 15, right: 5)
        convierteteVendedorButton.layer.cornerRadius = 10
        
        if usuario.cocinero{
            
            convierteteVendedorButton.isHidden = true
        }
    }

    
    @objc func abrirMenu(){
                        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let menuVC = storyboard.instantiateViewController(identifier: "menuVC")
        
        menuVC.modalPresentationStyle = .overFullScreen
        
        self.present(menuVC, animated: false, completion: nil)
    }
    
    //    MARK:- redireccionamientos menu
    
    func formasDePago(){
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let formasDePagoVC = storyboard.instantiateViewController(identifier: "formasDePagoVC")
        
        self.navigationController?.pushViewController(formasDePagoVC, animated: true)
    }
    
    func codigosPromocionales(){
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let codigosPromoVC = storyboard.instantiateViewController(identifier: "codigosPromoVC")
        
        self.navigationController?.pushViewController(codigosPromoVC, animated: true)
    }
    
    @IBAction func formasDePagoUnwind(segue: UIStoryboardSegue){
        
        if let segue = segue as? UIStoryboardSegueWithCompletion{
            
            segue.completion = {
                
                self.formasDePago()
            }
        }
    }
    
    @IBAction func codigosPromocionalesUnwind(segue: UIStoryboardSegue){
        
        if let segue = segue as? UIStoryboardSegueWithCompletion{
            
            segue.completion = {
                
                self.codigosPromocionales()
            }
        }
    }
}
