//
//  LoginViewController.swift
//  comi
//
//  Created by Francisco Escobar on 6/1/20.
//  Copyright © 2020 Francisco Escobar. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var campoCorreo: UITextField! {
        didSet{
            campoCorreo.tintColor = UIColor.lightGray
            campoCorreo.setIconCorreo(#imageLiteral(resourceName: "correo"))
        }
    }
    @IBOutlet weak var campoPassword: UITextField!{
        didSet{
            campoPassword.tintColor = UIColor.lightGray
            campoPassword.setIconPass(#imageLiteral(resourceName: "candado"))
        }
    }
    @IBOutlet weak var iniciarSesionButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        prepararUI()
    }
    
    func prepararUI(){
        
        campoCorreo.layer.cornerRadius = 15
        campoCorreo.layer.masksToBounds = true
        
        campoPassword.layer.cornerRadius = 15
        campoPassword.layer.masksToBounds = true
        
        iniciarSesionButton.layer.cornerRadius = 13
        iniciarSesionButton.layer.shadowColor = UIColor(red: 0.1960, green: 0.1960, blue: 0.1960, alpha: 1).cgColor
        iniciarSesionButton.layer.shadowOpacity = 0.5
        iniciarSesionButton.layer.shadowOffset = CGSize(width: 0, height: 5)
        iniciarSesionButton.layer.shadowRadius = 5
    
    }
}

extension UITextField{
    
    func setIconCorreo(_ image:UIImage){
        
        let iconoImageView = UIImageView(frame: CGRect(x: 15, y: 18, width: 21.5, height: 15))
        iconoImageView.image = image
        
        let contenedorIconoView: UIView = UIView(frame: CGRect(x: 30, y: 0, width: 40, height: 50))
        contenedorIconoView.addSubview(iconoImageView)
        
        leftView = contenedorIconoView
        leftViewMode = .always
    }
    
    func setIconPass(_ image:UIImage){
        
        let iconoImageView = UIImageView(frame: CGRect(x: 18, y: 13, width: 15.75, height: 21))
        iconoImageView.image = image
        
        let contenedorIconoView: UIView = UIView(frame: CGRect(x: 30, y: 0, width: 40, height: 50))
        contenedorIconoView.addSubview(iconoImageView)
        
        leftView = contenedorIconoView
        leftViewMode = .always
    }
}

@IBDesignable class RoundButton: UIButton {

    @IBInspectable var cornerRadius: CGFloat = 15 {
      didSet {
         setNeedsLayout()
      }
   }
   @IBInspectable var shadowColor: UIColor = UIColor.black {
      didSet {
         setNeedsLayout()
      }
   }
   @IBInspectable var shadowOffsetWidth: CGFloat = 0.0 {
      didSet {
         setNeedsLayout()
      }
   }
   @IBInspectable var shadowOffsetHeight: CGFloat = 2 {
      didSet {
         setNeedsLayout()
      }
   }
   @IBInspectable var shadowOpacity: Float = 0.30 {
      didSet {
         setNeedsLayout()
      }
   }
   @IBInspectable var shadowRadius: CGFloat = 5.0 {
      didSet {
         setNeedsLayout()
      }
   }
   private var shadowLayer: CAShapeLayer = CAShapeLayer() {
      didSet {
         setNeedsLayout()
      }
   }
   override func layoutSubviews() {
      super.layoutSubviews()
      layer.cornerRadius = cornerRadius
      shadowLayer.path = UIBezierPath(roundedRect: bounds,
         cornerRadius: cornerRadius).cgPath
      shadowLayer.fillColor = backgroundColor?.cgColor
      shadowLayer.shadowColor = shadowColor.cgColor
      shadowLayer.shadowPath = shadowLayer.path
      shadowLayer.shadowOffset = CGSize(width: shadowOffsetWidth,
         height: shadowOffsetHeight)
      shadowLayer.shadowOpacity = shadowOpacity
      shadowLayer.shadowRadius = shadowRadius
      layer.insertSublayer(shadowLayer, at: 0)
   }
}
