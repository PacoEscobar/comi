//
//  EntregasViewController.swift
//  comi
//
//  Created by Francisco Escobar on 04/07/20.
//  Copyright © 2020 Francisco Escobar. All rights reserved.
//

import UIKit

class EntregasViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    let puertos = PuertosPlatillo()
    
    var platillos: [[String: Any]] = [[String: Any]]()
    
    @IBOutlet weak var platillosTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        NotificationCenter.default.addObserver(self, selector: #selector(actualizarListaPlatillos), name: .entregas, object: nil)
        
        puertos.obtenerEntregas()
        
        let backButton = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        navigationItem.backBarButtonItem = backButton
        
    }
    
    @objc func actualizarListaPlatillos(_ notification:Notification){
                
        let platillosTopLevel = notification.userInfo as! [String:Any]
        
        platillos = platillosTopLevel["platillos"] as! [[String:Any]]
        platillosTableView.reloadData()
    }
    
    //MARK: - tableview
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return platillos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let celda = tableView.dequeueReusableCell(withIdentifier: "celdaPlatillo")!
        
        let usuario = platillos[indexPath.row]["usuario"] as! Usuario
        let platillo = platillos[indexPath.row]["platillo"] as! Platillo
        
        let sombraView = celda.viewWithTag(1)
        let fondoView = celda.viewWithTag(2)
        let platilloImageView = celda.viewWithTag(3) as! UIImageView
        let nombreLabel = celda.viewWithTag(4) as! UILabel
        let totalLabel = celda.viewWithTag(5) as! UILabel
        let envioLabel = celda.viewWithTag(6) as! UILabel
        
        sombraView?.layer.cornerRadius = 10
        sombraView?.layer.shadowColor = UIColor(red: 0.1960, green: 0.1960, blue: 0.1960, alpha: 1).cgColor
        sombraView?.layer.shadowOpacity = 0.2
        sombraView?.layer.shadowOffset = CGSize(width: 5, height: 5)
        sombraView?.layer.shadowRadius = 5
        
        fondoView?.layer.cornerRadius = 10
        fondoView?.layer.masksToBounds = true
        
        if platillo.fotoImage == UIImage(){
            
            puertos.descargarFoto(platillo: platillo, en: platilloImageView)
            
        }else{
            platilloImageView.image = platillo.fotoImage
        }
        
        nombreLabel.text = platillo.nombre
        
        if platillo.precio.truncatingRemainder(dividingBy: 1) == 0{
            
            totalLabel.text = String(format: "$%.0f",platillo.precio)
        }else{
        
            totalLabel.text = String(format: "$%.2f",platillo.precio)
        }
        
        if platillo.entrega == 1{
            
            envioLabel.text = "Un repartidor llegará por el platillo"
        }else if platillo.entrega == 2{
            
            envioLabel.text = "Tú te encargas del envío"
        }
        
        celda.selectionStyle = .none
        
        return celda
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let usuario = platillos[indexPath.row]["usuario"] as! Usuario
        let platillo = platillos[indexPath.row]["platillo"] as! Platillo
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let detallesEntregaVC = storyboard.instantiateViewController(identifier: "detallesEntregaVC") as DetallesEntregaViewController
        
        detallesEntregaVC.platillo = platillo
        detallesEntregaVC.usuario = usuario
        
        self.navigationController?.pushViewController(detallesEntregaVC, animated: true)
    }
    
}
