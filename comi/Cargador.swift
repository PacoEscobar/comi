//
//  Cargador.swift
//  comi
//
//  Created by Francisco Escobar on 02/07/20.
//  Copyright © 2020 Francisco Escobar. All rights reserved.
//

import Foundation
import UIKit

class Cargador {
    
    var ventaConfirmada = false
    var cicloImagenes = 2
    
    var imagenImageView = UIImageView()
    var fondo = UIView()
    
    init(){}
    
    init(viewController: UIViewController){
        
        fondo = UIView(frame: viewController.view.frame)
        fondo.backgroundColor = UIColor(red: 31, green: 29, blue: 56, a: 0.9)

        imagenImageView = UIImageView(frame: CGRect(x: (viewController.view.frame.size.width/2)-50, y: (viewController.view.frame.size.height/2)-50, width: 100, height: 100))
        imagenImageView.image = #imageLiteral(resourceName: "cargador_bebida")

        fondo.addSubview(imagenImageView)
        viewController.view.addSubview(fondo)
        
        cambiarImagen()
    }
    
    func cambiarImagen(){
        
        DispatchQueue.main.asyncAfter(deadline: .now()+1, execute: {
            
            UIView.animate(withDuration: 0.5, animations: {
                
                self.rollIn()
                
            }, completion: {termino in
                
                self.seleccionarImagen()
                
                
                
                if termino{
                    
                    UIView.animate(withDuration: 0.5, delay: 0, options: UIView.AnimationOptions.curveEaseIn, animations: { () -> Void in
                        
                        self.rollOut()
                    }, completion: {termino in
                        
                        self.cambiarImagen()
                    })
                }
            })
        })
    }
    
    func rollIn(){
        
        self.imagenImageView.transform = CGAffineTransform(scaleX: 0.02, y: 0.02)
        
        self.imagenImageView.transform = self.imagenImageView.transform.rotated(by: CGFloat(Double.pi))
        
        self.imagenImageView.transform = self.imagenImageView.transform.rotated(by: CGFloat(Double.pi))
    }
    
    func rollOut(){
        
        self.imagenImageView.transform = self.imagenImageView.transform.rotated(by: CGFloat(Double.pi))
        
        self.imagenImageView.transform = self.imagenImageView.transform.rotated(by: CGFloat(Double.pi))
        
        self.imagenImageView.transform = CGAffineTransform(scaleX: 1, y: 1)
    }
    
    func seleccionarImagen(){

        switch cicloImagenes {
        case 1:
            self.imagenImageView.image = UIImage(named: "cargador_bebida")!
        case 2:
            self.imagenImageView.image = UIImage(named: "cargador_plato")!
        case 3:
            self.imagenImageView.image = UIImage(named: "cargador_cocinero")!
        case 4:
            self.imagenImageView.image = UIImage(named: "cargador_hamburguesa")!
        default:
            self.imagenImageView.image = UIImage()
        }
        
        if self.cicloImagenes == 4{
            
            self.cicloImagenes = 1
        }else{
            
            self.cicloImagenes+=1
        }
    }
    
    func alto(){
        
        fondo.removeFromSuperview()
    }
}
