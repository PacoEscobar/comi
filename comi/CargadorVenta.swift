//
//  Cargador.swift
//  comi
//
//  Created by Francisco Escobar on 6/18/20.
//  Copyright © 2020 Francisco Escobar. All rights reserved.
//

import UIKit

class CargadorVenta: UIViewController{
    
    var ventaConfirmada = false
    var cicloImagenes = 2
    
    @IBOutlet weak var imagenImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        cambiarImagen()
        prepararUI()
    }
    
    func prepararUI(){
        
        NotificationCenter.default.addObserver(self, selector: #selector(ventaFinalizada), name: .platilloVendido, object: nil)
        
        let puertos = PuertosPlatillo()
        puertos.vender()
    }
    
    func cambiarImagen(){
        
        DispatchQueue.main.asyncAfter(deadline: .now()+1, execute: {
            
            UIView.animate(withDuration: 0.5, animations: {
                
                self.rollIn()
                
            }, completion: {termino in
                
                self.seleccionarImagen()
                
                
                
                if termino{
                    
                    UIView.animate(withDuration: 0.5, delay: 0, options: UIView.AnimationOptions.curveEaseIn, animations: { () -> Void in
                        
                        self.rollOut()
                    }, completion: {termino in
                        
                        self.cambiarImagen()
                    })
                }
            })
        })
    }
    
    func rollIn(){
        
        self.imagenImageView.transform = CGAffineTransform(scaleX: 0.02, y: 0.02)
        
        self.imagenImageView.transform = self.imagenImageView.transform.rotated(by: CGFloat(Double.pi))
        
        self.imagenImageView.transform = self.imagenImageView.transform.rotated(by: CGFloat(Double.pi))
    }
    
    func rollOut(){
        
        self.imagenImageView.transform = self.imagenImageView.transform.rotated(by: CGFloat(Double.pi))
        
        self.imagenImageView.transform = self.imagenImageView.transform.rotated(by: CGFloat(Double.pi))
        
        self.imagenImageView.transform = CGAffineTransform(scaleX: 1, y: 1)
    }
    
    func seleccionarImagen(){
        
        switch cicloImagenes {
        case 1:
            self.imagenImageView.image = UIImage(named: "cargador_bebida")!
        case 2:
            let puertos = PuertosPlatillo()
            if ventaConfirmada{
                
                puertos.vender()
                self.imagenImageView.image = UIImage(named: "cargador_plato")!
            }
        case 3:
            self.imagenImageView.image = UIImage(named: "cargador_cocinero")!
        case 4:
            self.imagenImageView.image = UIImage(named: "cargador_hamburguesa")!
        default:
            self.imagenImageView.image = UIImage()
        }
        
        if self.cicloImagenes == 4{
            
            self.cicloImagenes = 1
        }else{
            
            self.cicloImagenes+=1
        }
    }
    
    @objc func ventaFinalizada(){
                   
        ventaConfirmada = true
        self.performSegue(withIdentifier: "continuarVentaUnwind", sender: self)
    }
}
