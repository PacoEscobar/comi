//
//  NuevoPlatilloViewController.swift
//  comi
//
//  Created by Francisco Escobar on 6/22/20.
//  Copyright © 2020 Francisco Escobar. All rights reserved.
//

import UIKit

class NuevoPlatilloViewController: UIViewController, UITextViewDelegate, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    var entrega = 0 //1 repartidor, 2 yo lo llevo
    var cantidadEsNumero = false
    var precioEsNumero = false
    
    var usuario = Usuario()
    let platillo = Platillo()
    
    @IBOutlet weak var nombrePlatilloTextField: UITextField!{
        didSet{
            nombrePlatilloTextField.tintColor = UIColor.lightGray
            nombrePlatilloTextField.setMargin()
        }
    }
    @IBOutlet weak var descripcionPlatilloTextView: UITextView!{
        didSet{
            descripcionPlatilloTextView.tintColor = UIColor.lightGray
        }
    }
    @IBOutlet weak var sombraDescripcionPlatilloView: UIView!
    @IBOutlet weak var cantidadPlatillosTextField: UITextField!{
        didSet{
            cantidadPlatillosTextField.tintColor = UIColor.lightGray
            cantidadPlatillosTextField.setMargin()
        }
    }
    @IBOutlet weak var precioPlatillosTextField: UITextField!{
        didSet{
            precioPlatillosTextField.tintColor = UIColor.lightGray
            precioPlatillosTextField.setMargin()
        }
    }
    @IBOutlet weak var yoLoLlevoButton: UIButton!
    @IBOutlet weak var repartidorLoLlevaButton: UIButton!
    @IBOutlet weak var siguienteButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        prepararUI()
    }
    
    func prepararUI(){
        
        nombrePlatilloTextField.layer.cornerRadius = 5
        nombrePlatilloTextField.layer.shadowColor = UIColor(red: 0.1960, green: 0.1960, blue: 0.1960, alpha: 1).cgColor
        nombrePlatilloTextField.layer.shadowOpacity = 0.2
        nombrePlatilloTextField.layer.shadowOffset = CGSize(width: 2, height: 5)
        nombrePlatilloTextField.layer.shadowRadius = 5
        
        descripcionPlatilloTextView.layer.cornerRadius = 5
        sombraDescripcionPlatilloView.layer.cornerRadius = 5
        sombraDescripcionPlatilloView.layer.shadowColor = UIColor(red: 0.1960, green: 0.1960, blue: 0.1960, alpha: 1).cgColor
        sombraDescripcionPlatilloView.layer.shadowOpacity = 0.2
        sombraDescripcionPlatilloView.layer.shadowOffset = CGSize(width: 2, height: 5)
        sombraDescripcionPlatilloView.layer.shadowRadius = 5
        
        cantidadPlatillosTextField.layer.cornerRadius = 5
        cantidadPlatillosTextField.layer.shadowColor = UIColor(red: 0.1960, green: 0.1960, blue: 0.1960, alpha: 1).cgColor
        cantidadPlatillosTextField.layer.shadowOpacity = 0.2
        cantidadPlatillosTextField.layer.shadowOffset = CGSize(width: 2, height: 5)
        cantidadPlatillosTextField.layer.shadowRadius = 5
        
        precioPlatillosTextField.layer.cornerRadius = 5
        precioPlatillosTextField.layer.shadowColor = UIColor(red: 0.1960, green: 0.1960, blue: 0.1960, alpha: 1).cgColor
        precioPlatillosTextField.layer.shadowOpacity = 0.2
        precioPlatillosTextField.layer.shadowOffset = CGSize(width: 2, height: 5)
        precioPlatillosTextField.layer.shadowRadius = 5
        
        yoLoLlevoButton.layer.cornerRadius = 15
        yoLoLlevoButton.layer.shadowColor = UIColor(red: 0.1960, green: 0.1960, blue: 0.1960, alpha: 1).cgColor
        yoLoLlevoButton.layer.shadowOpacity = 0.3
        yoLoLlevoButton.layer.shadowOffset = CGSize(width: 3, height: 3)
        yoLoLlevoButton.layer.shadowRadius = 5
        
        repartidorLoLlevaButton.layer.cornerRadius = 15
        repartidorLoLlevaButton.layer.shadowColor = UIColor(red: 0.1960, green: 0.1960, blue: 0.1960, alpha: 1).cgColor
        repartidorLoLlevaButton.layer.shadowOpacity = 0.3
        repartidorLoLlevaButton.layer.shadowOffset = CGSize(width: 3, height: 3)
        repartidorLoLlevaButton.layer.shadowRadius = 5
        
        siguienteButton.layer.cornerRadius = 15
        siguienteButton.layer.shadowColor = UIColor(red: 0.1960, green: 0.1960, blue: 0.1960, alpha: 1).cgColor
        siguienteButton.layer.shadowOpacity = 0.3
        siguienteButton.layer.shadowOffset = CGSize(width: 3, height: 3)
        siguienteButton.layer.shadowRadius = 5
        
        descripcionPlatilloTextView.text = "Descripción del platillo"
        descripcionPlatilloTextView.textColor = UIColor.lightGray
                
        let backButton = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        navigationItem.backBarButtonItem = backButton
        
    }
    
    @IBAction func cambiarARepartidor(_ sender: Any) {
        
        if entrega != 1{
            
            repartidorLoLlevaButton.backgroundColor = UIColor(red: 0.839215, green: 0.525490, blue: 0.176470, alpha: 1)
            yoLoLlevoButton.backgroundColor = UIColor(white: 0.70, alpha: 1)
            entrega = 1
        }
    }
    
    @IBAction func cambiarAYoLoLlevo(_ sender: Any) {
        
        if entrega != 2{
            
            repartidorLoLlevaButton.backgroundColor = UIColor(white: 0.70, alpha: 1)
            yoLoLlevoButton.backgroundColor = UIColor(red: 0.839215, green: 0.525490, blue: 0.176470, alpha: 1)
            entrega = 2
        }
    }
    
    func agregarSignoPesos(textField: UITextField){
        
        let contenedorIconoView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
        
        let paddingLeft = UIView(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
        paddingLeft.tag = 1
        
        let label = UILabel(frame: CGRect(x: 10, y: 0, width: 10, height: 0))
        label.text = "$"
        label.textColor = UIColor(white: 0.325449, alpha: 1)
        label.alpha = 0
        label.tag = 2
        
        paddingLeft.addSubview(label)
        
        textField.rightView = contenedorIconoView
        textField.rightViewMode = .always
        
        textField.leftView = paddingLeft
        textField.leftViewMode = .always
        
        UIView.animate(withDuration: 0.2, animations: {
            
            label.alpha = 1
            label.frame = CGRect(x: 10, y: 0, width: 10, height: 10)
            label.sizeToFit()
        })
        
    }
    
    func quitarSignoPesos(textField:UITextField){
        
        let paddingLeft = textField.leftView?.viewWithTag(1)
        let label = paddingLeft?.viewWithTag(2) as! UILabel
        
        UIView.animate(withDuration: 0.2, animations: {
            
            label.alpha = 0
            label.frame = CGRect(x: 10, y: 0, width: 10, height: 0)
        })
    }
    
    @IBAction func tomarFoto(_ sender: Any) {
        
//        let pickerController = UIImagePickerController()
//        pickerController.delegate = self
//        pickerController.allowsEditing = true
//        pickerController.mediaTypes = ["public.image"]
//        pickerController.sourceType = .camera
        
//        self.present(pickerController, animated: true, completion: nil)
        
        let fotoPlatillo = #imageLiteral(resourceName: "link-pastelpapa")
        procederAResumen(fotoPlatillo: fotoPlatillo)
    }
    
    func procederAResumen(fotoPlatillo: UIImage){
        
        let repositorioPlatillo = RepositorioPlatillo()
        
        let camposVacios = repositorioPlatillo.validarCamposLlenos(nombrePlatilloTextField: nombrePlatilloTextField, descripcionPlatilloTextView: descripcionPlatilloTextView, cantidadPlatillosTextField: cantidadPlatillosTextField, precioPlatillosTextField: precioPlatillosTextField)
                
        cantidadEsNumero = repositorioPlatillo.validarCantidad(textField: cantidadPlatillosTextField)
        precioEsNumero = repositorioPlatillo.validarNumeros(textField: precioPlatillosTextField)
                
        if !cantidadEsNumero{
            
            repositorioPlatillo.coloresError(textField: cantidadPlatillosTextField)
        }
        
        if !precioEsNumero{
            
            repositorioPlatillo.coloresError(textField: precioPlatillosTextField)
        }
                
        if camposVacios.count == 0 && cantidadEsNumero && precioEsNumero && entrega != 0{
            
            repositorioPlatillo.coloresDefault(textField: cantidadPlatillosTextField)
            repositorioPlatillo.coloresDefault(textField: precioPlatillosTextField)
            
            platillo.nombre = nombrePlatilloTextField.text!
            platillo.descripcion = descripcionPlatilloTextView.text
            platillo.cantidadDisponibles = Int(cantidadPlatillosTextField.text!)!
            platillo.precio = Double(precioPlatillosTextField.text!)!
            platillo.fotoImage = fotoPlatillo
            platillo.entrega = entrega
            
            self.performSegue(withIdentifier: "vistaPreviaSegue", sender: self)
        }else if entrega == 0{
            
            let alertController = UIAlertController(title: "", message: "Necesitas seleccionar quién llevará la comida", preferredStyle: .alert)
            let aceptarAction = UIAlertAction(title: "Aceptar", style: .default, handler: nil)
            
            alertController.addAction(aceptarAction)
            
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
//    MARK:- segue
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let vistaPreviaPlatilloVC = segue.destination as! VistaPreviaPlatilloViewController
        
        vistaPreviaPlatilloVC.platillo = platillo
    }
    
    
//MARK:- textview
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if textView.textColor == .lightGray{

            textView.text = ""
            textView.textColor = nombrePlatilloTextField.textColor
        }
        
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if textView.text == ""{
            
            textView.text = "Descripción del platillo"
            textView.textColor = .lightGray
        }
    }

    
//MARK:- textfield
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == precioPlatillosTextField{
            if textField.text == ""{
                
                agregarSignoPesos(textField: textField)
            }
        }
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        
        if textField == precioPlatillosTextField{
            if textField.text == ""{
                
                quitarSignoPesos(textField: textField)
            }
        }
        
        return true
    }
    
    
    //MARK:- image picker
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let imagenSeleccionada = info[.editedImage] as? UIImage{
            
            dismiss(animated: true, completion: nil)
        }
    }
}

// MARK: - extensiones

extension UITextField{
    func setMargin(){
        
        let contenedorIconoView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
        
        let paddingLeft = UIView(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
        
        rightView = contenedorIconoView
        rightViewMode = .always
        
        leftView = paddingLeft
        leftViewMode = .always
    }
    
    func setSignoPesos(){
        
        let contenedorIconoView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
        
        let paddingLeft = UIView(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
        
        let label = UILabel(frame: CGRect(x: 10, y: 0, width: 10, height: 10))
        label.text = "$"
        label.textColor = UIColor(white: 0.325449, alpha: 1)
        label.sizeToFit()

        paddingLeft.addSubview(label)
        
        rightView = contenedorIconoView
        rightViewMode = .always
        
        leftView = paddingLeft
        leftViewMode = .always
    }
    
}

extension UIColor{
    
    convenience init(red: Int, green: Int, blue: Int, a: CGFloat = 1.0) {
        self.init(red: CGFloat(red) / 255.0,green: CGFloat(green) / 255.0,blue: CGFloat(blue) / 255.0,alpha: a)
    }
}
