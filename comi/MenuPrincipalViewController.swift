//
//  MenuPrincipalViewController.swift
//  comi
//
//  Created by Francisco Escobar on 17/07/20.
//  Copyright © 2020 Francisco Escobar. All rights reserved.
//

import UIKit

class MenuPrincipalViewController: UIViewController {

    var frameInicialGeneral = CGRect()
    
    //-413 constraint para empezar
    @IBOutlet weak var constraintMenu: NSLayoutConstraint!
    @IBOutlet weak var fondoView: UIView!
    
    @IBOutlet weak var perfilImageView: UIImageView!
    @IBOutlet weak var perfilLabel: UILabel!
    
    @IBOutlet weak var formasDePagoButton: UIButton!
    @IBOutlet weak var formasDePagoLabel: UILabel!
    @IBOutlet weak var formasDePagoImageView: UIImageView!
    
    @IBOutlet weak var ayudaButton: UIButton!
    @IBOutlet weak var ayudaLabel: UILabel!
    @IBOutlet weak var ayudaImageView: UIImageView!
    
    @IBOutlet weak var codigoButton: UIButton!
    @IBOutlet weak var codigoLabel: UILabel!
    @IBOutlet weak var codigoImageView: UIImageView!
    
    @IBOutlet weak var cocinaButton: UIButton!
    @IBOutlet weak var cocinaLabel: UILabel!
    @IBOutlet weak var cocinaImageView: UIImageView!
    
    @IBOutlet weak var configuracionButton: UIButton!
    @IBOutlet weak var configuracionLabel: UILabel!
    @IBOutlet weak var configuracionImageView: UIImageView!
    
    @IBOutlet weak var cerrarSesionButton: UIButton!
    @IBOutlet weak var cerrarSesionLabel: UILabel!
    @IBOutlet weak var cerrarSesionImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        prepararUI()
        prepararGesturesRecognizer()
    }
    
    func prepararUI(){
        
        perfilImageView.layer.cornerRadius = 50
        perfilImageView.layer.borderColor = UIColor(red: 214, green: 134, blue: 65).cgColor
        perfilImageView.layer.borderWidth = 5
        
        frameInicialGeneral = fondoView.frame
        
        print("general iniical \(frameInicialGeneral)")
        
    }
    
    func prepararGesturesRecognizer(){
        
        let cerrarTGR = UITapGestureRecognizer(target: self, action: #selector(cerrarMenu))
        fondoView.addGestureRecognizer(cerrarTGR)
        
        let cerrarPGR = UIPanGestureRecognizer(target: self, action: #selector(cerrarMenuPan))
        fondoView.addGestureRecognizer(cerrarPGR)
    }
    
    func prepararInfoPerfil(){
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        UIView.animate(withDuration: 0.5, animations: {
            
            self.constraintMenu.constant = 0
            self.view.layoutIfNeeded()
        })
    }
    
    @objc func cerrarMenu(){
        
        UIView.animate(withDuration: 0.5, animations: {
            
            self.constraintMenu.constant = -self.view.frame.size.width
            self.view.layoutIfNeeded()
        },completion: { termino in
            
            if termino{
                self.dismiss(animated: false, completion: nil)
            }
        })
    }
    
    @objc func cerrarMenuPan(recognizer: UIPanGestureRecognizer){

        let centroInicial = fondoView.center
        let traslado = recognizer.translation(in: fondoView.superview)
        
        if recognizer.state == .changed{
            
            let nuevoCentro = CGPoint(x:centroInicial.x + traslado.x, y: centroInicial.y)
            
            if nuevoCentro.x < centroInicial.x{
                                                                
                //fondoView.frame = CGRect(x: traslado.x, y: frameInicial.origin.y, width: frameInicial.size.width, height: frameInicial.size.height)
                
                constraintMenu.constant = traslado.x

            }
        }
        
        if recognizer.state == .ended{
            
            if traslado.x <= -170{
                
                cerrarMenu()
            }else{
                
                regresarMenuAbierto()
            }
        }
    }
    
    func regresarMenuAbierto(){
        print("regresa a \(frameInicialGeneral.origin.y)")
        UIView.animate(withDuration: 0.2, animations: {
            
            //self.fondoView.frame = CGRect(x: 0, y: self.frameInicialGeneral.origin.y, width: self.fondoView.frame.size.width, height: self.fondoView.frame.size.height)
            
            self.constraintMenu.constant = 0
            
        })
    }
    
    //MARK: Metodos botones menu
    
    
    @IBAction func cambiarBotonFormasDePago(_ sender: Any) {
        
        formasDePagoButton.layer.cornerRadius = 15
        formasDePagoButton.backgroundColor = UIColor(red: 214, green: 134, blue: 65)
        formasDePagoLabel.textColor = .white
        formasDePagoImageView.image = UIImage(named: "formas_pago_b")
        
        
        formasDePagoButton.superview!.sendSubviewToBack(formasDePagoButton)
    }
    @IBAction func cancelarBotonFormasDePago(_ sender: Any) {
        
        formasDePagoButton.layer.cornerRadius = 15
        formasDePagoButton.backgroundColor = UIColor(white: 1, alpha: 0)
        formasDePagoLabel.textColor = .black
        formasDePagoImageView.image = UIImage(named: "formas_pago")
        
        
        formasDePagoButton.superview!.bringSubviewToFront(formasDePagoButton)
    }
    
    @IBAction func irFormasDePago(_ sender: Any) {
        
        UIView.animate(withDuration: 0.5, animations: {
            
            self.constraintMenu.constant = -self.view.frame.size.width
            self.view.layoutIfNeeded()
        },completion: { termino in
            
            if termino{
                
                self.performSegue(withIdentifier: "formasDePagoUnwind", sender: self)
            }
        })
    }
    
    @IBAction func cambiarBotonAyuda(_ sender: Any) {
        
        ayudaButton.layer.cornerRadius = 15
        ayudaButton.backgroundColor = UIColor(red: 214, green: 134, blue: 65)
        ayudaLabel.textColor = .white
        ayudaImageView.image = UIImage(named: "ayuda_b")
        
        
        ayudaButton.superview!.sendSubviewToBack(ayudaButton)
    }
    @IBAction func cancelarBotonAyuda(_ sender: Any) {
        
        ayudaButton.layer.cornerRadius = 15
        ayudaButton.backgroundColor = UIColor(white: 1, alpha: 0)
        ayudaLabel.textColor = .black
        ayudaImageView.image = UIImage(named: "ayuda")
        
        
        ayudaButton.superview!.bringSubviewToFront(ayudaButton)
    }
    @IBAction func irAyuda(_ sender: Any) {
        
        UIView.animate(withDuration: 0.5, animations: {
            
            self.constraintMenu.constant = -self.view.frame.size.width
            self.view.layoutIfNeeded()
        },completion: { termino in
            
            if termino{
                //                va al vc
            }
        })
    }
    
    @IBAction func cambiarBotonCodigos(_ sender: Any) {
        
        codigoButton.layer.cornerRadius = 15
        codigoButton.backgroundColor = UIColor(red: 214, green: 134, blue: 65)
        codigoLabel.textColor = .white
        codigoImageView.image = UIImage(named: "codigo_b")
        
        
        codigoButton.superview!.sendSubviewToBack(codigoButton)
    }
    @IBAction func cancelarBotonCodigos(_ sender: Any) {
        
        codigoButton.layer.cornerRadius = 15
        codigoButton.backgroundColor = UIColor(white: 0, alpha: 0)
        codigoLabel.textColor = .black
        codigoImageView.image = UIImage(named: "codigo")
        
        codigoButton.superview!.bringSubviewToFront(codigoButton)
    }
    @IBAction func irCodigos(_ sender: Any) {
        
        UIView.animate(withDuration: 0.5, animations: {
            
            self.constraintMenu.constant = -self.view.frame.size.width
            self.view.layoutIfNeeded()
        },completion: { termino in
            
            if termino{
                self.performSegue(withIdentifier: "codigosPromocionalesUnwind", sender: self)
            }
        })
    }
    
    
    @IBAction func cambiarBotonCocina(_ sender: Any) {
        
        cocinaButton.layer.cornerRadius = 15
        cocinaButton.backgroundColor = UIColor(red: 214, green: 134, blue: 65)
        cocinaLabel.textColor = .white
        cocinaImageView.image = UIImage(named: "cocina_comi_b")
        
        
        cocinaButton.superview!.sendSubviewToBack(cocinaButton)
    }
    @IBAction func cancelarBotonCocina(_ sender: Any) {
        
        cocinaButton.layer.cornerRadius = 15
        cocinaButton.backgroundColor = UIColor(white: 0, alpha: 0)
        cocinaLabel.textColor = .black
        cocinaImageView.image = UIImage(named: "cocina_comi")
        
        
        cocinaButton.superview!.bringSubviewToFront(cocinaButton)
    }
    @IBAction func irCocina(_ sender: Any) {
        
        UIView.animate(withDuration: 0.5, animations: {
            
            self.constraintMenu.constant = -self.view.frame.size.width
            self.view.layoutIfNeeded()
        },completion: { termino in
            
            if termino{
                //                va al vc
            }
        })
    }
    
    
    @IBAction func cambiarBotonConfiguracion(_ sender: Any) {
        
        configuracionButton.layer.cornerRadius = 15
        configuracionButton.backgroundColor = UIColor(red: 214, green: 134, blue: 65)
        configuracionLabel.textColor = .white
        configuracionImageView.image = UIImage(named: "configuracion_b")
        
        
        configuracionButton.superview!.sendSubviewToBack(configuracionButton)
    }
    @IBAction func cancelarBotonConfiguracion(_ sender: Any) {
        
        configuracionButton.layer.cornerRadius = 15
        configuracionButton.backgroundColor = UIColor(white: 0, alpha: 0)
        configuracionLabel.textColor = .black
        configuracionImageView.image = UIImage(named: "configuracion")
        
        
        configuracionButton.superview!.bringSubviewToFront(configuracionButton)
    }
    @IBAction func irConfiguracion(_ sender: Any) {
        
        UIView.animate(withDuration: 0.5, animations: {
            
            self.constraintMenu.constant = -self.view.frame.size.width
            self.view.layoutIfNeeded()
        },completion: { termino in
            
            if termino{
                //                va al vc
            }
        })
    }
    
    @IBAction func cambiarBotonCerrarSesion(_ sender: Any) {
        
        cerrarSesionButton.layer.cornerRadius = 15
        cerrarSesionButton.backgroundColor = UIColor(red: 214, green: 134, blue: 65)
        cerrarSesionLabel.textColor = .white
        cerrarSesionImageView.image = UIImage(named: "cerrarsesion_b")
        
        
        cerrarSesionButton.superview!.sendSubviewToBack(cerrarSesionButton)
    }
    @IBAction func cancelarBotonCerrarSesion(_ sender: Any) {
        
        cerrarSesionButton.layer.cornerRadius = 15
        cerrarSesionButton.backgroundColor = UIColor(white: 0, alpha: 0)
        cerrarSesionLabel.textColor = .black
        cerrarSesionImageView.image = UIImage(named: "cerrarsesion")
        
        
        cerrarSesionButton.superview!.bringSubviewToFront(cerrarSesionButton)
    }
    @IBAction func irCerrarSesion(_ sender: Any) {
        
        UIView.animate(withDuration: 0.5, animations: {
            
            self.constraintMenu.constant = -self.view.frame.size.width
            self.view.layoutIfNeeded()
        },completion: { termino in
            
            if termino{
                //                va al vc
            }
        })
    }
    
}
