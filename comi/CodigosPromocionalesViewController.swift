//
//  CodigosPromocionalesViewController.swift
//  comi
//
//  Created by Francisco Escobar on 26/07/20.
//  Copyright © 2020 Francisco Escobar. All rights reserved.
//

import UIKit

class CodigosPromocionalesViewController: UIViewController {

    @IBOutlet weak var imagenView: UIView!
    @IBOutlet weak var codigoTextField: UITextField!{
        
        didSet{
            
            let leftView = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: 2))
            
            codigoTextField.leftView = leftView
            codigoTextField.leftViewMode = .always
        }
    }
    @IBOutlet weak var aplicarButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        prepararUI()
    }
    
    func prepararUI(){
        
        imagenView.layer.cornerRadius = 40
        imagenView.layer.shadowColor = UIColor(red: 0.1960, green: 0.1960, blue: 0.1960, alpha: 1).cgColor
        imagenView.layer.shadowOpacity = 0.2
        imagenView.layer.shadowOffset = CGSize(width: 2, height: 5)
        imagenView.layer.shadowRadius = 5
        
        codigoTextField.layer.cornerRadius = 5
        codigoTextField.layer.shadowColor = UIColor(red: 0.1960, green: 0.1960, blue: 0.1960, alpha: 1).cgColor
        codigoTextField.layer.shadowOpacity = 0.3
        codigoTextField.layer.shadowOffset = CGSize(width: 3, height: 3)
        codigoTextField.layer.shadowRadius = 5
        
        aplicarButton.layer.cornerRadius = 10
        aplicarButton.layer.shadowColor = UIColor(red: 0.1960, green: 0.1960, blue: 0.1960, alpha: 1).cgColor
        aplicarButton.layer.shadowOpacity = 0.3
        aplicarButton.layer.shadowOffset = CGSize(width: 3, height: 3)
        aplicarButton.layer.shadowRadius = 5
    }
}
