//
//  TabBarPrincipalViewController.swift
//  comi
//
//  Created by Francisco Escobar on 6/8/20.
//  Copyright © 2020 Francisco Escobar. All rights reserved.
//

import UIKit

class TabBarPrincipalViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        let defaults = UserDefaults.standard
        let usuario = Usuario()
        let fuenteAttributes = [NSAttributedString.Key.font: UIFont(name: "Poppins", size: 14)]
        let carrito:[Platillo] = [Platillo]()
        
        defaults.set(carrito, forKey: "carrito")
        
        UITabBarItem.appearance().setTitleTextAttributes(fuenteAttributes as [NSAttributedString.Key : Any], for: .normal)
        usuario.cocinero = true
        if usuario.cocinero{
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let entregasVC = storyboard.instantiateViewController(identifier: "entregasNavVC")
            entregasVC.tabBarItem.title = "Entregas"
            entregasVC.tabBarItem.image = UIImage(named: "tab_entregas")
            
            self.viewControllers?.insert(entregasVC, at: 1)
        }
    }
}
