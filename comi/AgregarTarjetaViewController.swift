//
//  AgregarTarjetaViewController.swift
//  comi
//
//  Created by Francisco Escobar on 22/07/20.
//  Copyright © 2020 Francisco Escobar. All rights reserved.
//

import UIKit

class AgregarTarjetaViewController: UIViewController, UITextFieldDelegate {

    var keyboardHeight:CGFloat = 0.0
    var tecladoSeleccionado = UITextField()
    
    var tarjeta = Tarjeta()
    
    @IBOutlet weak var nombreTextField: UITextField!{
        
        didSet{
            
            let leftView = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: 2))
            
            nombreTextField.leftView = leftView
            nombreTextField.leftViewMode = .always
        }
    }
    
    @IBOutlet weak var numeroTextField: UITextField!{
        
        didSet{
            
            let leftView = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: 2))
            
            numeroTextField.leftView = leftView
            numeroTextField.leftViewMode = .always
        }
    }
    @IBOutlet weak var cvvTextField: UITextField!{
        
        didSet{
            
            let leftView = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: 2))
            
            cvvTextField.leftView = leftView
            cvvTextField.leftViewMode = .always
        }
    }
    @IBOutlet weak var fechaVencimientoTextField: UITextField!{
        
        didSet{
            
            let leftView = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: 2))
            
            fechaVencimientoTextField.leftView = leftView
            fechaVencimientoTextField.leftViewMode = .always
        }
    }
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var guardarButton: UIButton!
    
    @IBOutlet weak var scrollViewBottomConstraint: NSLayoutConstraint!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        prepararUI()
        crearGestureRecognizers()
    }
    
    func prepararUI(){

        nombreTextField.layer.cornerRadius = 5
        nombreTextField.layer.shadowColor = UIColor(red: 0.1960, green: 0.1960, blue: 0.1960, alpha: 1).cgColor
        nombreTextField.layer.shadowOpacity = 0.3
        nombreTextField.layer.shadowOffset = CGSize(width: 3, height: 3)
        nombreTextField.layer.shadowRadius = 5
        
        numeroTextField.layer.cornerRadius = 5
        numeroTextField.layer.shadowColor = UIColor(red: 0.1960, green: 0.1960, blue: 0.1960, alpha: 1).cgColor
        numeroTextField.layer.shadowOpacity = 0.3
        numeroTextField.layer.shadowOffset = CGSize(width: 3, height: 3)
        numeroTextField.layer.shadowRadius = 5
        
        cvvTextField.layer.cornerRadius = 5
        cvvTextField.layer.shadowColor = UIColor(red: 0.1960, green: 0.1960, blue: 0.1960, alpha: 1).cgColor
        cvvTextField.layer.shadowOpacity = 0.3
        cvvTextField.layer.shadowOffset = CGSize(width: 3, height: 3)
        cvvTextField.layer.shadowRadius = 5
        
        fechaVencimientoTextField.layer.cornerRadius = 5
        fechaVencimientoTextField.layer.shadowColor = UIColor(red: 0.1960, green: 0.1960, blue: 0.1960, alpha: 1).cgColor
        fechaVencimientoTextField.layer.shadowOpacity = 0.3
        fechaVencimientoTextField.layer.shadowOffset = CGSize(width: 3, height: 3)
        fechaVencimientoTextField.layer.shadowRadius = 5
        
        guardarButton.layer.cornerRadius = 5
        guardarButton.layer.shadowColor = UIColor(red: 0.1960, green: 0.1960, blue: 0.1960, alpha: 1).cgColor
        guardarButton.layer.shadowOpacity = 0.3
        guardarButton.layer.shadowOffset = CGSize(width: 3, height: 3)
        guardarButton.layer.shadowRadius = 5
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillShow),
            name: UIResponder.keyboardWillShowNotification,
            object: nil
        )
        
    }
    
    func crearGestureRecognizers(){
        
        let esconderTelcadoTGR = UITapGestureRecognizer(target: self, action: #selector(esconderTeclado))
        self.view.addGestureRecognizer(esconderTelcadoTGR)
    }
    
    @objc func esconderTeclado(){
        print("teclado seleccionado \(tecladoSeleccionado)")
        if tecladoSeleccionado.isFirstResponder{
            
            tecladoSeleccionado.resignFirstResponder()
            UIView.animate(withDuration: 0.25, animations: {
                
                self.scrollViewBottomConstraint.constant = 0
                self.view.layoutIfNeeded()
            })
        }
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
                
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            keyboardHeight = keyboardRectangle.height
            
           // if heightScrollViewConstraint.constant == 0.0{
                
                UIView.animate(withDuration: 0.25, animations: {
                    
                    var rectEsperado = self.tecladoSeleccionado.frame
                    
                    rectEsperado.origin.y = rectEsperado.origin.y+20
                    
                    self.scrollViewBottomConstraint.constant = self.keyboardHeight
                    self.view.layoutIfNeeded()
                    self.scrollView.scrollRectToVisible(rectEsperado, animated: false)
                })
           // }
        }
    }
    
    @IBAction func guardar(_ sender: Any) {
        
        if nombreTextField.text != "" && numeroTextField.text != "" && cvvTextField.text != "" && fechaVencimientoTextField.text != ""{
            
            let repositorioTarjetas = RepositorioTarjetas()
                        
            tarjeta.owner.nombre = nombreTextField.text!
            tarjeta.numero = Int(numeroTextField.text!) ?? 0
            tarjeta.codigo = Int(cvvTextField.text!) ?? 0
            tarjeta.fecha = repositorioTarjetas.stringADateUnix(fecha: fechaVencimientoTextField.text!)
                    
            let _ = Cargador()
            
            self.performSegue(withIdentifier: "tarjetaAgregada", sender: self)
        }
    }
    
    
    //MARK: - textfield
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        tecladoSeleccionado = textField
        
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
                
        /*if tecladoSeleccionado.isFirstResponder{
            
            UIView.animate(withDuration: 0.5, animations: {
                
                self.view.frame = CGRect(x: self.view.frame.origin.x, y: self.view.frame.origin.y-100, width: self.view.frame.size.width, height: self.view.frame.size.height)
            })
        }*/
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
              
        if fechaVencimientoTextField.isFirstResponder{
            
            let isBackSpace = strcmp(string, "\\b")
            if (isBackSpace == -92) {
                return true
            }
            
            let repositorioTarjetas = RepositorioTarjetas()
            
            fechaVencimientoTextField.text = repositorioTarjetas.fechaAPlaceholder(fecha: fechaVencimientoTextField.text!, nuevoChar: string)
            
            return false
        }
        
        return true
    }
    
    //MARK:- navegacion
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.destination is MetodosDePagoViewController{
                        
            let metodosDePagoVC = segue.destination as! MetodosDePagoViewController
            
            print("tarjetas \(metodosDePagoVC.tarjetas)")
            
            metodosDePagoVC.tarjetas.append(tarjeta)
        }
    }
}
