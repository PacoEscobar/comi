//
//  DetallesPlatilloViewController.swift
//  comi
//
//  Created by Francisco Escobar on 6/8/20.
//  Copyright © 2020 Francisco Escobar. All rights reserved.
//

import UIKit
import MapKit

class DetallesPlatilloViewController: UIViewController, MKMapViewDelegate {

    var pagoTarjeta = false
    
    var platillo = Platillo()
    var usuario = Usuario()
        
    @IBOutlet weak var sombraPlatilloView: UIView!
    @IBOutlet weak var platilloImageView: UIImageView!
    @IBOutlet weak var perfilImageView: UIImageView!
    @IBOutlet weak var nombrePerfilLabel: UILabel!
    @IBOutlet weak var estrella1ImageView: UIImageView!
    @IBOutlet weak var estrella2ImageView: UIImageView!
    @IBOutlet weak var estrella3ImageView: UIImageView!
    @IBOutlet weak var estrella4ImageView: UIImageView!
    @IBOutlet weak var estrella5ImageView: UIImageView!
    @IBOutlet weak var nombrePlatilloLabel: UILabel!
    @IBOutlet weak var precioLabel: UILabel!
    @IBOutlet weak var descripcionLabel: UILabel!
    @IBOutlet weak var disponiblesLabel: UILabel!
    @IBOutlet weak var pedirButton: UIButton!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var sombraMapView: UIView!
    
    //MARK: -
    
    override func viewDidLoad() {
        super.viewDidLoad()

        prepararUI()
    }
    
    func prepararUI(){
        
        let backButton = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        navigationItem.backBarButtonItem = backButton
        
        let repositorioUsuario = RepositorioUsuario()
        
        perfilImageView.layer.cornerRadius = 25
        
        repositorioUsuario.ponerEstrellas(estrella1: estrella1ImageView, estrella2: estrella2ImageView, estrella3: estrella3ImageView, estrella4: estrella4ImageView, estrella5: estrella5ImageView, usuario: platillo.vendedor)
        
        sombraPlatilloView.layer.cornerRadius = 10
        platilloImageView.layer.cornerRadius = 10
        sombraPlatilloView.layer.shadowColor = UIColor(red: 0.1960, green: 0.1960, blue: 0.1960, alpha: 1).cgColor
        sombraPlatilloView.layer.shadowOpacity = 0.3
        sombraPlatilloView.layer.shadowOffset = CGSize(width: 5, height: 5)
        sombraPlatilloView.layer.shadowRadius = 5
        
        platilloImageView.image = platillo.fotoImage
        nombrePlatilloLabel.text = platillo.nombre
        
        perfilImageView.image = platillo.vendedor.fotoImage
        nombrePerfilLabel.text = platillo.vendedor.nombre
        
        descripcionLabel.text = platillo.descripcion
        
        if platillo.precio.truncatingRemainder(dividingBy: 1) == 0{
            
            precioLabel.text = String(format: "$%.0f",platillo.precio)
        }else{
        
            precioLabel.text = String(format: "$%.2f",platillo.precio)
        }
        
        disponiblesLabel.text = "\(platillo.cantidadDisponibles) disponibles"
        
        self.navigationItem.title = platillo.nombre
        
        pedirButton.layer.cornerRadius = 15
        pedirButton.layer.shadowColor = UIColor(red: 0.1960, green: 0.1960, blue: 0.1960, alpha: 1).cgColor
        pedirButton.layer.shadowOpacity = 0.3
        pedirButton.layer.shadowOffset = CGSize(width: 3, height: 3)
        pedirButton.layer.shadowRadius = 5
        
        
        sombraMapView.layer.shadowColor = UIColor(red: 0.1960, green: 0.1960, blue: 0.1960, alpha: 1).cgColor
        sombraMapView.layer.shadowOpacity = 0.3
        sombraMapView.layer.shadowOffset = CGSize(width: 3, height: 3)
        sombraMapView.layer.shadowRadius = 5
        
        let location = CLLocation(latitude: platillo.ubicacion.object(forKey: "latitud") as! Double, longitude: platillo.ubicacion.object(forKey: "longitud") as! Double)
        
        let region = MKCoordinateRegion(center: location.coordinate, latitudinalMeters: 5000, longitudinalMeters: 5000)
        
       
        mapView.setRegion(region, animated: true)
        
        let zoomRange = MKMapView.CameraZoomRange(maxCenterCoordinateDistance: 5000)
        mapView.setCameraZoomRange(zoomRange, animated: true)
        
        let radio = 600.0
        let circulo = MKCircle(center: location.coordinate, radius: radio)
        
        mapView.addOverlay(circulo)
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        
        let circulo = MKCircleRenderer(overlay: overlay)
        
        circulo.strokeColor = UIColor(red: 0.8399215, green: 0.525490, blue: 0.254901, alpha: 1)
        circulo.fillColor = UIColor(red: 0.8399215, green: 0.525490, blue: 0.254901, alpha: 0.4)
        circulo.lineWidth = 2.0

        return circulo
    }
    
    
    //MARK: - NAVEGACION
    
    //MARK: segues
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.destination.isKind(of: RealizarPedidoViewController.self){
            
            let realizarPedidoVC = segue.destination as! RealizarPedidoViewController
            
            realizarPedidoVC.platillo = platillo
        }
    }
    
    //MARK: unwinds
    
    @IBAction func unwindSeleccionEfectivo(segue: UIStoryboardSegue){
        
        if let segue = segue as? UIStoryboardSegueWithCompletion{
            
            segue.completion = {
                
                self.pagoTarjeta = false
                self.performSegue(withIdentifier: "realizarPedidoSegue", sender: self)
                
            }
        }
    }
    
    @IBAction func unwindSeleccionTarjeta(segue: UIStoryboardSegue){
        
        if let segue = segue as? UIStoryboardSegueWithCompletion{
            
            segue.completion = {
                
                self.pagoTarjeta = true
                self.performSegue(withIdentifier: "realizarPedidoSegue", sender: self)
                
            }
        }
    }
}
