//
//  UIStoryboardSegueWithCompletion.swift
//  comi
//
//  Created by Francisco Escobar on 6/13/20.
//  Copyright © 2020 Francisco Escobar. All rights reserved.
//

import UIKit

class UIStoryboardSegueWithCompletion: UIStoryboardSegue {
    
    var completion: (() -> Void)?
    
    override func perform() {
        
        super.perform()
        
        if let completion = completion{
            
            completion()
        }
    }

}
