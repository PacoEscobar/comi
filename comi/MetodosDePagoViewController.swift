//
//  MetodosDePagoViewController.swift
//  comi
//
//  Created by Francisco Escobar on 22/07/20.
//  Copyright © 2020 Francisco Escobar. All rights reserved.
//

import UIKit

class MetodosDePagoViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var tarjetas = [Tarjeta]()
    
    @IBOutlet weak var tarjetasTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        prepararUI()
    }
    
    func prepararUI(){
        
        NotificationCenter.default.addObserver(self, selector: #selector(actualizarTarjetas), name: .tarjetas, object: nil)
        
        let puertos = PuertosTarjeta()
        puertos.obtenerTarjetas(usuario: Usuario())
        
        let backButton = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        navigationItem.backBarButtonItem = backButton
    }
    
    @objc func actualizarTarjetas(_ notification:Notification){
        
        let tarjetasTopLevel = notification.userInfo as! [String:Any]
        
        tarjetas = tarjetasTopLevel["tarjetas"] as! [Tarjeta]
        
        tarjetasTableView.reloadData()
    }
    
    //MARK:- tableview
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return tarjetas.count+2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var celda = UITableViewCell()
        
        if indexPath.row == tarjetas.count{
            
            celda = tableView.dequeueReusableCell(withIdentifier: "celdaEfectivo")!
            
            let vista = celda.viewWithTag(1)
            
            vista?.layer.cornerRadius = 15
            vista?.layer.shadowColor = UIColor(red: 0.1960, green: 0.1960, blue: 0.1960, alpha: 1).cgColor
            vista?.layer.shadowOpacity = 0.3
            vista?.layer.shadowOffset = CGSize(width: 3, height: 3)
            vista?.layer.shadowRadius = 5
        }else if indexPath.row == (tarjetas.count+1){
            
            celda = tableView.dequeueReusableCell(withIdentifier: "celdaAgregar")!
        }else{
            
            celda = tableView.dequeueReusableCell(withIdentifier: "celdaTarjeta")!
            
            let repositorioTarjetas = RepositorioTarjetas()
            let tarjeta = tarjetas[indexPath.row]
            let vista = celda.viewWithTag(1)
            let tarjetaImageView = celda.viewWithTag(2) as! UIImageView
            let numeroLabel = celda.viewWithTag(3) as! UILabel
            let principalLabel = celda.viewWithTag(4) as! UILabel
            
            
            vista?.layer.cornerRadius = 15
            vista?.layer.shadowColor = UIColor(red: 0.1960, green: 0.1960, blue: 0.1960, alpha: 1).cgColor
            vista?.layer.shadowOpacity = 0.3
            vista?.layer.shadowOffset = CGSize(width: 3, height: 3)
            vista?.layer.shadowRadius = 5
            
            numeroLabel.text = repositorioTarjetas.stringifyNumero(tarjeta: tarjeta)
            
            if tarjeta.principal{
                principalLabel.isHidden = false
                principalLabel.layer.cornerRadius = 9
                principalLabel.layer.masksToBounds = true
            }else{
                principalLabel.isHidden = true
            }
            
            switch tarjeta.marca {
            case 0:
                tarjetaImageView.image = UIImage(named: "formas_pago")
                break
            case 1:
                tarjetaImageView.image = UIImage(named: "visa")
                break
            case 2:
                tarjetaImageView.image = UIImage(named: "mastercard")
                break
            default:
                break
            }
        }
        
        celda.selectionStyle = .none
        
        return celda
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == (tarjetas.count+1){
            
            self.performSegue(withIdentifier: "agregarTarjetaSegue", sender: self)
        }
    }
    
    //MARK:- unwinds
    
    @IBAction func TarjetaAgregadaUnwind(segue: UIStoryboardSegue){
        
        if let segue = segue as? UIStoryboardSegueWithCompletion{
            
            segue.completion = {

                self.tarjetasTableView.reloadData()
            }
        }
    }
}
