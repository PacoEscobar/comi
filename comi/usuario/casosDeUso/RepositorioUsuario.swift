//
//  RepositorioUsuario.swift
//  comi
//
//  Created by Francisco Escobar on 6/18/20.
//  Copyright © 2020 Francisco Escobar. All rights reserved.
//

import Foundation
import UIKit

class RepositorioUsuario{
    
    func ponerEstrellas(estrella1: UIImageView, estrella2: UIImageView, estrella3: UIImageView, estrella4: UIImageView, estrella5: UIImageView, usuario:Usuario){
        
        switch usuario.calificacion {
        case 1:
            estrella1.image = UIImage(named:"estrella_filled")
            estrella2.image = UIImage(named:"estrella_empty")
            estrella3.image = UIImage(named:"estrella_empty")
            estrella4.image = UIImage(named:"estrella_empty")
            estrella5.image = UIImage(named:"estrella_empty")
        case 2:
            estrella1.image = UIImage(named:"estrella_filled")
            estrella2.image = UIImage(named:"estrella_filled")
            estrella3.image = UIImage(named:"estrella_empty")
            estrella4.image = UIImage(named:"estrella_empty")
            estrella5.image = UIImage(named:"estrella_empty")
        case 3:
            estrella1.image = UIImage(named:"estrella_filled")
            estrella2.image = UIImage(named:"estrella_filled")
            estrella3.image = UIImage(named:"estrella_filled")
            estrella4.image = UIImage(named:"estrella_empty")
            estrella5.image = UIImage(named:"estrella_empty")
        case 4:
            estrella1.image = UIImage(named:"estrella_filled")
            estrella2.image = UIImage(named:"estrella_filled")
            estrella3.image = UIImage(named:"estrella_filled")
            estrella4.image = UIImage(named:"estrella_filled")
            estrella5.image = UIImage(named:"estrella_empty")
        case 5:
            estrella1.image = UIImage(named:"estrella_filled")
            estrella2.image = UIImage(named:"estrella_filled")
            estrella3.image = UIImage(named:"estrella_filled")
            estrella4.image = UIImage(named:"estrella_filled")
            estrella5.image = UIImage(named:"estrella_filled")
        
        default:
            break
        }
    }
    
    func obtenerMisDatos()->Usuario{
        
        let defaults = UserDefaults.standard
        let usuario = Usuario()
        
//        usuario.id = defaults.integer(forKey: "id")
//        usuario.nombre = defaults.string(forKey: "nombre")!
//        usuario.cocinero = defaults.bool(forKey: "vendedor")
  
        return usuario
    }
}
