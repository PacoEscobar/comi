//
//  Puertos.swift
//  comi
//
//  Created by Francisco Escobar on 6/7/20.
//  Copyright © 2020 Francisco Escobar. All rights reserved.
//

import Foundation
import UIKit

class PuertosUsuario{
    
    func descargarFoto(usuario:Usuario, en: UIImageView){
        
        descargarImagen(from: usuario.foto, completionHandler: {data in
            
            usuario.fotoImage = UIImage(data: data!)!
            DispatchQueue.main.async {
                
                en.image = usuario.fotoImage
            }
        })
    }
    
    func descargarImagen(from urlString: String, completionHandler: @escaping (_ data: Data?) -> ()) {
        let session = URLSession.shared
        let url = URL(string: urlString)
            
        let dataTask = session.dataTask(with: url!) { (data, response, error) in
            if error != nil {
                print("Error fetching the image! 😢")
                completionHandler(nil)
            } else {
                completionHandler(data)
            }
        }
            
        dataTask.resume()
    }
}
