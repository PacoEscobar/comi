
import Foundation
import UIKit

class Usuario{
    
    var id = 0
    var nombre = ""
    var apellido = ""
    var genero = 0
    var foto = ""
    var correo = ""
    var ubicacion = NSMutableDictionary()
    var cocinero = false
    var calificacion = 0.0
    
    var fotoImage = UIImage()
    
}
