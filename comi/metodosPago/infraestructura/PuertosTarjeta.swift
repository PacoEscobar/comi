//
//  PuertosTarjeta.swift
//  comi
//
//  Created by Francisco Escobar on 24/07/20.
//  Copyright © 2020 Francisco Escobar. All rights reserved.
//

import Foundation

class PuertosTarjeta{
    
    func guardarTarjeta(tarjeta: Tarjeta){}
    
    func obtenerTarjetas(usuario:Usuario){
        
        var arrayTarjetas = [Tarjeta]()
        
        let tarjeta = Tarjeta()
        
        tarjeta.owner.nombre = "Francisco Escobar"
        tarjeta.numero = 411111111111111
        tarjeta.fecha = 1595640375
        tarjeta.codigo = 123
        tarjeta.principal = true
        tarjeta.marca = 1
        
        arrayTarjetas.append(tarjeta)
        
        let tarjeta2 = Tarjeta()
        
        tarjeta2.owner.nombre = "Francisco Escobar"
        tarjeta2.numero = 411111111123464
        tarjeta2.fecha = 1595640375
        tarjeta2.codigo = 123
        tarjeta2.principal = false
        tarjeta2.marca = 2
        
        arrayTarjetas.append(tarjeta2)
        
        let tarjeta3 = Tarjeta()
        
        tarjeta3.owner.nombre = "Francisco Escobar"
        tarjeta3.numero = 411111111153296
        tarjeta3.fecha = 1595640375
        tarjeta3.codigo = 123
        tarjeta3.principal = false
        
        arrayTarjetas.append(tarjeta3)
        
        let tarjetas: [String: Any] = ["tarjetas":arrayTarjetas]

        NotificationCenter.default.post(name:.tarjetas, object: nil, userInfo: tarjetas)
    }
}
