//
//  RepositorioTarjetas.swift
//  comi
//
//  Created by Francisco Escobar on 23/07/20.
//  Copyright © 2020 Francisco Escobar. All rights reserved.
//

import Foundation

class RepositorioTarjetas{
    
    func fechaAPlaceholder(fecha:String, nuevoChar:String)->String{

        var fechaFormateada = ""
                   
        if fecha.count == 5{
            
            fechaFormateada = fecha
        }else{
            
            if fecha.count == 0{
                
                let mes = Int(nuevoChar) ?? -1
                
                if mes >= 2{
                    
                    fechaFormateada = "0\(nuevoChar)/"
                }else if mes == 0 || mes == 1{
                    
                    fechaFormateada = "\(mes)"
                }
            }else{
                
                let fechaSeparada = fecha.split(separator: "/")
                let mes = fechaSeparada[0]
                
                if fechaSeparada.count == 1{
                    if mes.count == 1 && mes == "0"{
                        
                        let nuevoInt = Int(nuevoChar) ?? -1
                        
                        if nuevoInt >= 1{
                            
                            fechaFormateada = "0\(nuevoInt)/"
                        }
                    }else if mes.count == 1 && mes == "1"{
                        
                        let nuevoInt = Int(nuevoChar) ?? -1
                        
                        if nuevoInt == 0 || nuevoInt == 1 || nuevoInt == 2{
                            
                            fechaFormateada = "1\(nuevoInt)/"
                        }else if nuevoInt >= 2{
                            
                            fechaFormateada = "1"
                        }
                    }else if mes.count == 2{
                        
                        let nuevoInt = Int(nuevoChar) ?? -1
                        
                        if nuevoInt >= 2{
                            
                            fechaFormateada = "\(mes)/\(nuevoInt)"
                        }else{
                            
                            fechaFormateada = "\(mes)/2"
                        }
                    }
                }else if fechaSeparada.count == 2{
                    
                    let year = fechaSeparada[1]
                    
                    if year.count == 1{
                        
                        let nuevoInt = Int(nuevoChar) ?? -1
                        
                        if nuevoInt != -1{
                            fechaFormateada = "\(fecha)\(nuevoInt)"
                        }else{
                            fechaFormateada = fecha
                        }
                    }
                }
            }
            
        }
        
        return fechaFormateada
    }
    
    func mes(fecha:String)->Int{
        
        let fechaSeparada = fecha.split(separator: "/")
        let mes = fechaSeparada[0]
        
        return Int(mes) ?? 0
    }
    
    func year(fecha:String)->Int{
    
        let fechaSeparada = fecha.split(separator: "/")
        let year = "20\(fechaSeparada[1])"
        
        return Int(year) ?? 0
    }
    
    func stringADateUnix(fecha:String)->Double{
        
        var date = Date()
        var dateComponents = DateComponents()
        
        dateComponents.day = 1
        dateComponents.month = mes(fecha: fecha)
        dateComponents.year = year(fecha: fecha)
        
        date = Calendar.current.date(from: dateComponents)!
        
        return date.timeIntervalSince1970
    }
    
    func stringifyNumero(tarjeta: Tarjeta)->String{
        
        let numero = "\(tarjeta.numero)"
        var numeroFormateado = ""
        
        let inicio = numero.index(numero.endIndex, offsetBy: -4)
        let rango = inicio..<numero.endIndex
        let ultimosDigitos = numero[rango]
        
        for _ in 0..<(numero.count-4) {
            
            numeroFormateado = "\(numeroFormateado)*"
        }
        
        numeroFormateado = "\(numeroFormateado)\(ultimosDigitos)"
        
        return numeroFormateado
    }
}
