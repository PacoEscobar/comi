//
//  Tarjeta.swift
//  comi
//
//  Created by Francisco Escobar on 23/07/20.
//  Copyright © 2020 Francisco Escobar. All rights reserved.
//

import Foundation

class Tarjeta{
    
    var numero = 0
    var fecha = 0.0
    var codigo = 0
    var owner = Usuario()
    var principal = false
    var marca = 0 //0 - no se sabe/otro, 1 - visa, 2 - mastercard
}
