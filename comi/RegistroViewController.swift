//
//  RegistroViewController.swift
//  comi
//
//  Created by Francisco Escobar on 6/2/20.
//  Copyright © 2020 Francisco Escobar. All rights reserved.
//

import UIKit

class RegistroViewController: UIViewController, UITextFieldDelegate {

    var error = false
    var keyboardHeight: CGFloat = 0.0
    
    var textFieldSeleccionado = UITextField()
    
    @IBOutlet weak var nombreTextField: UITextField!{
        
        didSet{
            
            let leftView = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: 2))
            
            nombreTextField.leftView = leftView
            nombreTextField.leftViewMode = .always
        }
    }
    @IBOutlet weak var apellidoTextField: UITextField!{
        
        didSet{
            
            let leftView = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: 2))
            
            apellidoTextField.leftView = leftView
            apellidoTextField.leftViewMode = .always
        }
    }
    @IBOutlet weak var correoTextField: UITextField!{
        
        didSet{
            
            let leftView = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: 2))
            
            correoTextField.leftView = leftView
            correoTextField.leftViewMode = .always
        }
    }
    @IBOutlet weak var passwordTextField: UITextField!{
        
        didSet{
            
            let leftView = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: 2))
            
            passwordTextField.leftView = leftView
            passwordTextField.leftViewMode = .always
        }
    }
    
    @IBOutlet weak var registrateButton: UIButton!
    @IBOutlet weak var mensajeErrorView: UIView!
    @IBOutlet weak var mensajeErrorTopConstraint: NSLayoutConstraint!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        prepararUI()
    }
    
    func prepararUI(){
        
        print("color actual textfield \(String(describing: correoTextField.textColor))")
        
        nombreTextField.layer.cornerRadius = 5
        nombreTextField.layer.shadowColor = UIColor(red: 0.1960, green: 0.1960, blue: 0.1960, alpha: 1).cgColor
        nombreTextField.layer.shadowOpacity = 0.2
        nombreTextField.layer.shadowOffset = CGSize(width: 2, height: 5)
        nombreTextField.layer.shadowRadius = 5
        
        apellidoTextField.layer.cornerRadius = 5
        apellidoTextField.layer.shadowColor = UIColor(red: 0.1960, green: 0.1960, blue: 0.1960, alpha: 1).cgColor
        apellidoTextField.layer.shadowOpacity = 0.2
        apellidoTextField.layer.shadowOffset = CGSize(width: 2, height: 5)
        apellidoTextField.layer.shadowRadius = 5
        
        correoTextField.layer.cornerRadius = 5
        correoTextField.layer.shadowColor = UIColor(red: 0.1960, green: 0.1960, blue: 0.1960, alpha: 1).cgColor
        correoTextField.layer.shadowOpacity = 0.2
        correoTextField.layer.shadowOffset = CGSize(width: 2, height: 5)
        correoTextField.layer.shadowRadius = 5
        
        passwordTextField.layer.cornerRadius = 5
        passwordTextField.layer.shadowColor = UIColor(red: 0.1960, green: 0.1960, blue: 0.1960, alpha: 1).cgColor
        passwordTextField.layer.shadowOpacity = 0.2
        passwordTextField.layer.shadowOffset = CGSize(width: 2, height: 5)
        passwordTextField.layer.shadowRadius = 5
        
        registrateButton.layer.cornerRadius = 15
        //registrateButton.layer.cornerRadius = 13
        registrateButton.layer.shadowColor = UIColor(red: 0.1960, green: 0.1960, blue: 0.1960, alpha: 1).cgColor
        registrateButton.layer.shadowOpacity = 0.4
        registrateButton.layer.shadowOffset = CGSize(width: 0, height: 3)
        registrateButton.layer.shadowRadius = 5
        
        mensajeErrorView.layer.cornerRadius = 5
        mensajeErrorView.layer.shadowColor = UIColor(red: 0.1960, green: 0.1960, blue: 0.1960, alpha: 1).cgColor
        mensajeErrorView.layer.shadowOpacity = 0.5
        mensajeErrorView.layer.shadowOffset = CGSize(width: 2, height: 5)
        mensajeErrorView.layer.shadowRadius = 5
        
        NotificationCenter.default.addObserver(self,
        selector: #selector(handle(keyboardShowNotification:)),
        name: UIResponder.keyboardDidShowNotification,
        object: nil)
        
        let cerrarTecladoTGR = UITapGestureRecognizer(target: self, action: #selector(cerrarTeclado))
        
        self.view.addGestureRecognizer(cerrarTecladoTGR)
    }

    @IBAction func atras(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func registrarUsuario(_ sender: Any) {
        
        let nombre = nombreTextField.text
        let apellido = apellidoTextField.text
        let correo = correoTextField.text
        let password = passwordTextField.text
        
        if nombre != "" && apellido != "" && correo != "" && password != ""{
            
            let correo = correoTextField.text
            
            let rango = NSRange(location: 0, length: correo!.utf16.count)
            let regex = try! NSRegularExpression(pattern: "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}")

            if regex.firstMatch(in: correo!, options:[], range: rango) != nil{
                
                correoTextField.layer.borderWidth = 0
                cerrarMensajeError(self)
            }else{

                correoTextField.layer.borderWidth = 2
                correoTextField.layer.borderColor = UIColor(red: 0.905882, green: 0.243137, blue: 0.333333, alpha: 1).cgColor
                
                UIView.animate(withDuration: 0.3, animations: {
                    
                    self.mensajeErrorTopConstraint.constant = 45
                    self.mensajeErrorView.alpha = 1
                    self.view.layoutSubviews()
                })
            }
        }
    }
    
    @IBAction func cerrarMensajeError(_ sender: Any) {
        
        UIView.animate(withDuration: 0.3, animations: {
            
            self.mensajeErrorTopConstraint.constant = 0
            self.mensajeErrorView.alpha = 0
            self.view.layoutSubviews()
        })
    }
    
    @objc
    private func handle(keyboardShowNotification notification: Notification) {
       
        if let userInfo = notification.userInfo,

            let rectanguloTeclado = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect {
            
            if keyboardHeight != rectanguloTeclado.height{
                
                keyboardHeight = rectanguloTeclado.height
                
                if textFieldSeleccionado == correoTextField || textFieldSeleccionado == passwordTextField{
                    
                    DispatchQueue.main.async {
                        UIView.animate(withDuration: 0.4, animations: {
                            
                            self.view.frame.origin.y = -self.keyboardHeight
                        })
                    }
                }
            }
        }
    }
    
    @objc func cerrarTeclado(){
        
        print("cerrar teclado \(textFieldSeleccionado)")
        
        textFieldSeleccionado.resignFirstResponder()
        
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.1, animations: {
                
                self.view.frame.origin.y = 0
            })
        }
    }
    
    // MARK: - textfield delegates
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        textFieldSeleccionado = textField
        
        if keyboardHeight != 0.0{
            
            if textField == correoTextField || textField == passwordTextField{
                
                DispatchQueue.main.async {
                    UIView.animate(withDuration: 0.4, animations: {
                        
                        self.view.frame.origin.y = -self.keyboardHeight
                    })
                }
            }else{
                
                if textField == nombreTextField || textField == apellidoTextField{
                    
                    DispatchQueue.main.async {
                        UIView.animate(withDuration: 0.1, animations: {
                            
                            self.view.frame.origin.y = 0
                        })
                    }
                }
            }
        }
    }
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        
        if textField == correoTextField{
            
            error = false
            correoTextField.layer.borderWidth = 0
            cerrarMensajeError(self)
        }
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        
        if textField == correoTextField{
            let correo = correoTextField.text
            
            let rango = NSRange(location: 0, length: correo!.utf16.count)
            let regex = try! NSRegularExpression(pattern: "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}")
            
            if regex.firstMatch(in: correo!, options:[], range: rango) == nil{
                
                error = true
                
                correoTextField.layer.borderWidth = 2
                correoTextField.layer.borderColor = UIColor(red: 0.905882, green: 0.243137, blue: 0.333333, alpha: 1).cgColor
            }
            
        }
        return true
    }
    
}
