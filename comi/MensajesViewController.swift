//
//  MensajesViewController.swift
//  comi
//
//  Created by Francisco Escobar on 14/07/20.
//  Copyright © 2020 Francisco Escobar. All rights reserved.
//

import UIKit

class MensajesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var yo = Usuario()
    var otro = Usuario()
    
    var keyboardHeight:CGFloat = 0.0
    var mensajes = [Mensaje]()
    var keyboardIsHidding = false
        
    @IBOutlet weak var mensajesTableView: UITableView!
    @IBOutlet weak var campoMensajeView: UIView!
    @IBOutlet weak var campoMensajeSombraView: UIView!
    @IBOutlet weak var campoMensajeTextField: UITextField!
    
    @IBOutlet weak var campoMensajeTextFieldConstraint: NSLayoutConstraint!
    @IBOutlet weak var mensajesConstraintBottom: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        prepararNotificadores()
        prepararUI()
    }
    
    func prepararUI(){
        
        mensajesTableView.keyboardDismissMode = .interactive
        
        campoMensajeView.layer.cornerRadius = 20
        campoMensajeView.layer.masksToBounds = true
        
        campoMensajeSombraView?.layer.cornerRadius = 20
        campoMensajeSombraView?.layer.shadowColor = UIColor(red: 0.1960, green: 0.1960, blue: 0.1960, alpha: 1).cgColor
        campoMensajeSombraView?.layer.shadowOpacity = 0.2
        campoMensajeSombraView?.layer.shadowOffset = CGSize(width: 5, height: 5)
        campoMensajeSombraView?.layer.shadowRadius = 5
        
        let puertos = PuertosMensajes()
        puertos.obtenerMensajes(yo: yo, otro: otro)
    }
    
    func prepararNotificadores(){
        
        NotificationCenter.default.addObserver(self, selector: #selector(actualizarMensajes), name: .mensajes, object: nil)
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillShow),
            name: UIResponder.keyboardWillShowNotification,
            object: nil
        )
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardDidShow(_:)),
            name: UIResponder.keyboardDidShowNotification,
            object: nil
        )
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)

    }
    
    @objc func actualizarMensajes(_ notification:Notification){
    
        let mensajesTopLevel = notification.userInfo as! [String:Any]
    
        mensajes = mensajesTopLevel["mensajes"] as! [Mensaje]
        
        mensajesTableView.reloadData()
        mensajesTableView.contentInset.bottom = 100
        
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            keyboardHeight = keyboardRectangle.height

            mensajesConstraintBottom.constant = keyboardHeight-50
            
        }
    }
    
    @objc func keyboardDidShow(_ notification: Notification){
        
        mensajesTableView.scrollToRow(at: IndexPath(row: mensajes.count-1, section: 0), at: .bottom, animated: true)
    }
    
    @objc func keyboardChangeFrame(_ notification: Notification){
        
        print("cambio frame")
    }
    @objc func keyboardWillHide(_ notification: Notification){
        
        print("se hace true \(String(describing: notification.userInfo))")
        
        keyboardIsHidding = true
    }
    
    @IBAction func enviarMensaje(_ sender: Any) {
        
        let mensajeNuevo = Mensaje()
        
        mensajeNuevo.texto = campoMensajeTextField.text!
        mensajeNuevo.owner = 1
        mensajeNuevo.fecha = Date().timeIntervalSince1970
        
        mensajes.append(mensajeNuevo)
        
        mensajesTableView.performBatchUpdates({
            
            self.mensajesTableView.insertRows(at: [IndexPath(row: self.mensajes.count-1, section: 0)], with: .bottom)
        }, completion: nil)
        
        mensajesTableView.scrollToRow(at: IndexPath(row: self.mensajes.count-1, section: 0), at: .bottom, animated: true)
        
        UIView.animate(withDuration: 0.3, animations: {
            
            self.campoMensajeTextFieldConstraint.constant = self.campoMensajeTextFieldConstraint.constant - 20
            
            self.campoMensajeTextField.alpha = 0
            self.view.layoutIfNeeded()
        }, completion: { termino in
            
            if termino{
                
                self.campoMensajeTextField.text = ""
                self.campoMensajeTextFieldConstraint.constant = 0
                self.campoMensajeTextField.alpha = 1
            }
        })
    }
    
    
//    MARK:- tableview
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return mensajes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var celda = UITableViewCell()
        
        let mensaje = mensajes[indexPath.row]
        
        if mensaje.owner == 0{
            
            celda =  tableView.dequeueReusableCell(withIdentifier: "mensajeOtro")!
        }else{
            
            celda =  tableView.dequeueReusableCell(withIdentifier: "mensajeMio")!
        }
        
        let fondoView = celda.viewWithTag(1)
        fondoView?.layer.cornerRadius = 10
                   
        let mensajeLabel = celda.viewWithTag(2) as! UILabel
        mensajeLabel.text = mensaje.texto
        
        let fecha = Date(timeIntervalSince1970: TimeInterval(mensaje.fecha))
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = .short
        dateFormatter.timeZone = .current
        
        let fechaLabel = celda.viewWithTag(3) as! UILabel
        fechaLabel.text = dateFormatter.string(from: fecha)
        
        return celda
    }
}
