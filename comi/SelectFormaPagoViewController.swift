//
//  SelectFormaPagoViewController.swift
//  comi
//
//  Created by Francisco Escobar on 6/12/20.
//  Copyright © 2020 Francisco Escobar. All rights reserved.
//

import UIKit

class SelectFormaPagoViewController: UIViewController {

    @IBOutlet weak var fondoView: UIView!
    @IBOutlet weak var fondoCajaRegistradoraView: UIView!
    @IBOutlet weak var cajaRegistradoraImageView: UIImageView!
    @IBOutlet weak var contenedorRestanteView: UIView!
    
    @IBOutlet weak var fondoBotonEfectivo: UIView!
    @IBOutlet weak var fondoBotonTarjeta: UIView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        prepararUI()
    }
    
    func prepararUI(){
        
       
        fondoView.layer.cornerRadius = 15
        fondoView.layer.borderWidth = 0.1
        fondoView.layer.borderColor = UIColor(red: 0.784313, green: 0.784313, blue: 0.784313, alpha: 1).cgColor
        
        fondoView.layer.shadowColor = UIColor(red: 0.1960, green: 0.1960, blue: 0.1960, alpha: 1).cgColor
        fondoView.layer.shadowOpacity = 0.3
        fondoView.layer.shadowOffset = CGSize(width: 3, height: 3)
        fondoView.layer.shadowRadius = 5
        
        fondoCajaRegistradoraView.layer.cornerRadius = 32.5
        fondoCajaRegistradoraView.layer.shadowColor = UIColor(red: 0.1960, green: 0.1960, blue: 0.1960, alpha: 1).cgColor
        fondoCajaRegistradoraView.layer.shadowOpacity = 0.4
        fondoCajaRegistradoraView.layer.shadowOffset = CGSize(width: 2, height: 2)
        fondoCajaRegistradoraView.layer.shadowRadius = 3
        
        
        
        
        fondoBotonEfectivo.layer.cornerRadius = 10
        fondoBotonEfectivo.layer.shadowColor = UIColor(red: 0.1960, green: 0.1960, blue: 0.1960, alpha: 1).cgColor
        fondoBotonEfectivo.layer.shadowOpacity = 0.3
        fondoBotonEfectivo.layer.shadowOffset = CGSize(width: 3, height: 3)
        fondoBotonEfectivo.layer.shadowRadius = 3
        
        
        
        
        fondoBotonTarjeta.layer.cornerRadius = 10
        fondoBotonTarjeta.layer.shadowColor = UIColor(red: 0.1960, green: 0.1960, blue: 0.1960, alpha: 1).cgColor
        fondoBotonTarjeta.layer.shadowOpacity = 0.3
        fondoBotonTarjeta.layer.shadowOffset = CGSize(width: 3, height: 3)
        fondoBotonTarjeta.layer.shadowRadius = 3
        
        
        
        let efectivoTGR = UITapGestureRecognizer(target: self, action: #selector(efectivo))
        
        let tarjetaTGR = UITapGestureRecognizer(target: self, action: #selector(tarjeta))
        
        fondoBotonEfectivo.addGestureRecognizer(efectivoTGR)
        
        fondoBotonTarjeta.addGestureRecognizer(tarjetaTGR)
        
    }
    
    @IBAction func cerrar(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func efectivo(){
        
        self.performSegue(withIdentifier: "efectivoUnwind", sender: self)
    }
    
    @objc func tarjeta(){
        
        self.performSegue(withIdentifier: "tarjetaUnwind", sender: self)
    }
}
