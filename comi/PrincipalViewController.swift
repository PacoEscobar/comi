//
//  PrincipalViewController.swift
//  comi
//
//  Created by Francisco Escobar on 6/4/20.
//  Copyright © 2020 Francisco Escobar. All rights reserved.
//

import UIKit

class PrincipalViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
        
    let puerto = PuertosPlatillo()
    var usuario = Usuario()
    
    var platillos = [Platillo]()
    var platillosListos = [Platillo]()
    var platillosProgramados = [Platillo]()
    private let notificationCenter = NotificationCenter()
    
    
    @IBOutlet weak var platillosSegmentControl: UISegmentedControl!
    @IBOutlet weak var platillosTableView: UITableView!
    
    @IBOutlet weak var busquedaTextField: UITextField!{
        didSet{
            busquedaTextField.tintColor = UIColor.lightGray
            busquedaTextField.setIcon(#imageLiteral(resourceName: "lupa"))
        }
    }
    
    //Constraints
    
    @IBOutlet weak var platillosSegmentControlTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var platillosSegmentControlBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var platillosSegmentControlHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var buscadorHeightConstraint: NSLayoutConstraint!
    
    
    //--------------------------
    override func viewDidLoad() {
        super.viewDidLoad()

        prepararUI()
    }
    
    func prepararUI(){
     
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: "Poppins-Medium", size: 18)!, NSAttributedString.Key.foregroundColor: UIColor.white]
        
        let botonMenu = UIBarButtonItem(image: #imageLiteral(resourceName: "burguer"), style: .done, target: self, action: #selector(abrirMenu))
        self.navigationItem.leftBarButtonItem = botonMenu
        
        busquedaTextField.layer.cornerRadius = 5
        busquedaTextField.layer.shadowColor = UIColor(red: 0.1960, green: 0.1960, blue: 0.1960, alpha: 1).cgColor
        busquedaTextField.layer.shadowOpacity = 0.2
        busquedaTextField.layer.shadowOffset = CGSize(width: 2, height: 5)
        busquedaTextField.layer.shadowRadius = 5
        
        NotificationCenter.default.addObserver(self, selector: #selector(actualizarListaPlatillos), name: .listaPlatillos, object: nil)
        
        puerto.obtenerPlatillos()
        
        let backButton = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        navigationItem.backBarButtonItem = backButton
        
        if usuario.cocinero{
            
            let nuevoPlatilloButton = UIBarButtonItem(image: UIImage(named: "nuevo_platillo"), style: .plain, target: self, action: #selector(nuevoPlatillo))
            navigationItem.rightBarButtonItem = nuevoPlatilloButton
        }
                
    }
    
    @objc func abrirMenu(){
                
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let menuVC = storyboard.instantiateViewController(identifier: "menuVC")
        
        menuVC.modalPresentationStyle = .overFullScreen
        
        self.present(menuVC, animated: false, completion: nil)
    }
    
    @objc func actualizarListaPlatillos(_ notification:Notification){
        
        let platillosTopLevel = notification.userInfo as! [String:Any]
        
        platillosListos = platillosTopLevel["platillosListos"] as! [Platillo]
        platillosProgramados = platillosTopLevel["platillosProgramados"] as! [Platillo]
        platillos = platillosTopLevel["platillosListos"] as! [Platillo]
        
        if platillos.count == 0{
            
            DispatchQueue.main.async {
                
            
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
             let noPlatillosVC = storyboard.instantiateViewController(identifier: "noPlatillosVC") as NoPlatillosViewController
             
             let viewControllers = [noPlatillosVC]
             self.navigationController?.setViewControllers(viewControllers, animated: false)
            }
        }else{
            platillosTableView.reloadData()
        }
    }
    
    @IBAction func cambiarListaPlatillos(_ sender: Any) {
                
        if platillosSegmentControl.selectedSegmentIndex == 0{
            
            platillos = platillosListos
        }else{
            
            platillos = platillosProgramados
        }
        
        platillosTableView.reloadData()
    }
    
    
    @objc func nuevoPlatillo(){
                
        self.performSegue(withIdentifier: "nuevoPlatilloSegue", sender: self)
    }
    
//    MARK:- redireccionamientos menu
    
    func formasDePago(){
        
        let backButton = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        navigationItem.backBarButtonItem = backButton
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let formasDePagoVC = storyboard.instantiateViewController(identifier: "formasDePagoVC")
        
        self.navigationController?.pushViewController(formasDePagoVC, animated: true)
    }
    
    //MARK:- imagenes
    
    func resizeImagen(imagen: UIImage)->UIImage{
        
        let porcentajeHeight = (200*100)/imagen.size.height
        
        let widthNuevo = (imagen.size.width*porcentajeHeight)/100
        
        let size = CGSize(width: widthNuevo, height: 200)
        UIGraphicsBeginImageContextWithOptions(size, false, 1.0)
        imagen.draw(in: CGRect(x: 0, y: 0, width: widthNuevo, height:200))
        let imagenNueva = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return imagenNueva!
    }
    
    func resizeImagen(imagen: UIImage, width: CGFloat, height: CGFloat)->UIImage{
        
        let porcentajeHeight = (height*100)/imagen.size.height
        
        let widthNuevo = (imagen.size.width*porcentajeHeight)/100
        
        let size = CGSize(width: widthNuevo, height: height)
        UIGraphicsBeginImageContextWithOptions(size, false, 1.0)
        imagen.draw(in: CGRect(x: 0, y: 0, width: widthNuevo, height:height))
        let imagenNueva = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return imagenNueva!
    }
    
    //MARK: - tableview
    
    func construirCeldaListo(celda:UITableViewCell, platillo:Platillo)->UITableViewCell{
                
        let repositorioUsuario = RepositorioUsuario()
        let fondoSombra = celda.viewWithTag(1)
        
        fondoSombra?.layer.cornerRadius = 10
        fondoSombra?.layer.shadowColor = UIColor(red: 0.1960, green: 0.1960, blue: 0.1960, alpha: 1).cgColor
        fondoSombra?.layer.shadowOpacity = 0.2
        fondoSombra?.layer.shadowOffset = CGSize(width: 5, height: 5)
        fondoSombra?.layer.shadowRadius = 5
        
        let fondoBordeRedondeado = celda.viewWithTag(2)
        fondoBordeRedondeado?.layer.cornerRadius = 10
        fondoBordeRedondeado?.layer.masksToBounds = true
                
        let fotoPlatillo = celda.viewWithTag(3) as! UIImageView
             
        if platillo.fotoImage == UIImage(){
            puerto.descargarFoto(platillo: platillo, en: fotoPlatillo)
        }else{
            fotoPlatillo.image = resizeImagen(imagen: platillo.fotoImage)
        }
        
        let fotoPerfil = celda.viewWithTag(4) as! UIImageView
        
        let estrella1 = celda.viewWithTag(5) as! UIImageView
        let estrella2 = celda.viewWithTag(6) as! UIImageView
        let estrella3 = celda.viewWithTag(7) as! UIImageView
        let estrella4 = celda.viewWithTag(8) as! UIImageView
        let estrella5 = celda.viewWithTag(9) as! UIImageView
        
        repositorioUsuario.ponerEstrellas(estrella1: estrella1, estrella2: estrella2, estrella3: estrella3, estrella4: estrella4, estrella5: estrella5, usuario: platillo.vendedor)
        
        if platillo.vendedor.fotoImage == UIImage(){
            puerto.descargarFoto(usuario: platillo.vendedor, en: fotoPerfil)
        }else{
            fotoPerfil.image = platillo.vendedor.fotoImage
        }
        fotoPerfil.layer.cornerRadius = 15
        
        let nombre = celda.viewWithTag(10) as! UILabel
        nombre.text = platillo.nombre
        nombre.frame = CGRect(x: nombre.frame.origin.x, y: nombre.frame.origin.y, width: nombre.frame.size.width, height: 28)
        
        let precio = celda.viewWithTag(11) as! UILabel
        
        if platillo.precio.truncatingRemainder(dividingBy: 1) == 0{
            
            precio.text = String(format: "$%.0f",platillo.precio)
        }else{
        
            precio.text = String(format: "$%.2f",platillo.precio)
        }
        
        return celda
    }
    
    func construirCeldaPedido(celda: UITableViewCell, platillo: Platillo)->UITableViewCell{
        
        let repositorioUsuario = RepositorioUsuario()
        let fondoSombra = celda.viewWithTag(1)
        
        fondoSombra?.layer.cornerRadius = 10
        fondoSombra?.layer.shadowColor = UIColor(red: 0.1960, green: 0.1960, blue: 0.1960, alpha: 1).cgColor
        fondoSombra?.layer.shadowOpacity = 0.2
        fondoSombra?.layer.shadowOffset = CGSize(width: 5, height: 5)
        fondoSombra?.layer.shadowRadius = 5
        
        let fondoBordeRedondeado = celda.viewWithTag(2)
        fondoBordeRedondeado?.layer.cornerRadius = 10
        fondoBordeRedondeado?.layer.masksToBounds = true
                
        let fotoPlatillo = celda.viewWithTag(3) as! UIImageView
                  
        if platillo.fotoImage == UIImage(){
            puerto.descargarFoto(platillo: platillo, width: 300, height: 300, en: fotoPlatillo)
        }else{
            fotoPlatillo.image = resizeImagen(imagen: platillo.fotoImage, width: 300, height: 300)
        }
        
        let fotoPerfil = celda.viewWithTag(4) as! UIImageView
        
        let estrella1 = celda.viewWithTag(5) as! UIImageView
        let estrella2 = celda.viewWithTag(6) as! UIImageView
        let estrella3 = celda.viewWithTag(7) as! UIImageView
        let estrella4 = celda.viewWithTag(8) as! UIImageView
        let estrella5 = celda.viewWithTag(9) as! UIImageView
        
        repositorioUsuario.ponerEstrellas(estrella1: estrella1, estrella2: estrella2, estrella3: estrella3, estrella4: estrella4, estrella5: estrella5, usuario: platillo.vendedor)
        
        if platillo.vendedor.fotoImage == UIImage(){
            puerto.descargarFoto(usuario: platillo.vendedor, en: fotoPerfil)
        }else{
            fotoPerfil.image = platillo.vendedor.fotoImage
        }
        fotoPerfil.layer.cornerRadius = 15
        
        let nombre = celda.viewWithTag(10) as! UILabel
        nombre.text = platillo.nombre
        nombre.frame = CGRect(x: nombre.frame.origin.x, y: nombre.frame.origin.y, width: nombre.frame.size.width, height: 28)
        
        let precio = celda.viewWithTag(11) as! UILabel
        
        if platillo.precio.truncatingRemainder(dividingBy: 1) == 0{
            
            precio.text = String(format: "$%.0f",platillo.precio)
        }else{
        
            precio.text = String(format: "$%.2f",platillo.precio)
        }
        
        let entregasLabel = celda.viewWithTag(12) as! UILabel
        
        
        let fecha = Date(timeIntervalSince1970: TimeInterval(platillo.horaDeEntrega))
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = .short
        dateFormatter.timeZone = .current

        entregasLabel.text = "Empiezan las entregas a las \(dateFormatter.string(from: fecha))"
        
        let tiempoRestanteLabel = celda.viewWithTag(13) as! UILabel
        let repositorioPlatillo = RepositorioPlatillo()
        let tiempoRestanteString = repositorioPlatillo.calcularTiempoRestantePlatillo(platillo: platillo)
        
        tiempoRestanteLabel.text = "Tiempo restante para hacer tu pedido: \(tiempoRestanteString)"
        
        return celda
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return platillos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var celda = UITableViewCell()
        
        if platillosSegmentControl.selectedSegmentIndex == 0{
            
            celda = tableView.dequeueReusableCell(withIdentifier: "celdaPlatillo")!
            
            let platillo = platillos[indexPath.row]
            
            celda = construirCeldaListo(celda: celda, platillo: platillo)
            
        }else{
            
            celda = tableView.dequeueReusableCell(withIdentifier: "celdaPlatilloProgramado")!
            
            let platillo = platillos[indexPath.row]
            
            celda = construirCeldaPedido(celda: celda, platillo: platillo)
        }
        
        celda.selectionStyle = .none
        
        return celda
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
                
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let detallesPlatilloVC = storyboard.instantiateViewController(identifier: "detallesPlatilloVC") as! DetallesPlatilloViewController
        
        let platillo = platillos[indexPath.row]
        
        detallesPlatilloVC.platillo = platillo
        
        self.navigationController?.pushViewController(detallesPlatilloVC, animated: true)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
                
        let rightView = busquedaTextField.rightView
        let imagenRightView = rightView?.viewWithTag(1)
        
        if scrollView.contentOffset.y > 0{
            
            UIView.animate(withDuration: 0.5, animations: {
                
                self.platillosSegmentControlTopConstraint.constant = 0
                self.platillosSegmentControlBottomConstraint.constant = 0
                self.platillosSegmentControlHeightConstraint.constant = 0
                self.platillosSegmentControl.alpha = 0
                self.view.layoutIfNeeded()
            }, completion: { termino in
                
                if termino{
                    
                    UIView.animate(withDuration: 0.3, animations: {
                        
                        self.buscadorHeightConstraint.constant = 0
                        rightView?.frame.size.height = 0
                        imagenRightView?.frame.size.height = 0
                        self.busquedaTextField.alpha = 0
                        self.view.layoutIfNeeded()
                    })
                }
            })
           
        }else if scrollView.contentOffset.y < 0{
            
            UIView.animate(withDuration: 0.3, animations: {
                
                self.platillosSegmentControlTopConstraint.constant = 21
                self.platillosSegmentControlBottomConstraint.constant = 10
                self.platillosSegmentControlHeightConstraint.constant = 30
                self.platillosSegmentControl.alpha = 1
                self.view.layoutIfNeeded()
            }, completion: { termino in
                
                if termino{
                    
                    UIView.animate(withDuration: 0.3, animations: {
                        
                        self.buscadorHeightConstraint.constant = 50
                        rightView?.frame.size.height = 50
                        imagenRightView?.frame.size.height = 25
                        self.busquedaTextField.alpha = 1
                        self.view.layoutIfNeeded()
                    })
                }
            })
        }
    }
    
    //MARK: -unwinds
    
    @IBAction func formasDePagoUnwind(segue: UIStoryboardSegue){
        
        if let segue = segue as? UIStoryboardSegueWithCompletion{
            
            segue.completion = {
                
                self.formasDePago()
            }
        }
    }
    
}

// MARK: - extensiones

extension UITextField{
    func setIcon(_ image:UIImage){
        
        let iconoImageView = UIImageView(frame: CGRect(x: 0, y: 12.5, width: 25, height: 25))
        iconoImageView.image = image
        iconoImageView.tag = 1
        
        let contenedorIconoView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
        contenedorIconoView.addSubview(iconoImageView)
                        
        let paddingLeft = UIView(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
        
        rightView = contenedorIconoView
        rightViewMode = .always
        
        leftView = paddingLeft
        leftViewMode = .always
    }
}
